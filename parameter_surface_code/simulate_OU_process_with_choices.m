function [y,choicelist,missedlist] = simulate_OU_process_with_choices(A,s,lambda,sigma,tt,thresh,rewardvec,snoise)

rewardon = find(abs(diff(rewardvec))>0&rewardvec(2:end)~=0);
rewardoff = find(abs(diff(rewardvec))>0&rewardvec(2:end)==0);
if length(rewardon)~=length(rewardoff)|any(rewardon>=rewardoff)
    error('Error in identifying reward onsets/offsets');
end

%simulate OU process
y(1) = 0;
choicelist = nan(0,3);
for i = 2:length(tt)
    if abs(y(i-1))>thresh
        %is there currently a reward available?
        if abs(rewardvec(i))==1 %reward is available
            if sign(y(i-1))==sign(rewardvec(i)) %correct response
                reward_outcome = 1;
            else %incorrect response
                reward_outcome = -1;
            end
            %which reward period are we in?
            current_reward_period = max(find(i>rewardon));
            if i>rewardoff(current_reward_period)
                error('mistake in identifying current reward period?');
            end
            
            %reset s to noise for remainder of reward period
            s(i:rewardoff(current_reward_period)) = ...
                snoise(i:rewardoff(current_reward_period));
            %set rewardvec to 0 for this peiod
            rewardvec(i:rewardoff(current_reward_period)) = 0; 
            
            %remove this reward from the list
            rewardon(current_reward_period) = [];
            rewardoff(current_reward_period) = [];
        else %no reward is available
            reward_outcome = 0;
        end
            
        
        choicelist(end+1,1:3) = [i sign(y(i-1)) reward_outcome];
        y(i) = 0;
    else
        dt = tt(i)-tt(i-1);
        dydt(i) = A*s(i)*dt+lambda*y(i-1)*dt + randn*sqrt(sigma^2*dt);
        y(i) = y(i-1) + dydt(i);
    end
end

% make a list of missed responses
missedlist = nan(0,2);
for i = 1:length(rewardoff)
    missedlist(i,:) = [rewardoff(i) rewardvec(rewardoff(i)-1)];
end

