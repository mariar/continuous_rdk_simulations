function [nCorrect,nFalseAlarms,nIncorrect,nMissed] = ...
    calculate_reward_rates(A,lambda,sigma,thresh,doplots,s,snoise,rewardvec)

if nargin<5
    doplots = 0;
end

%% 
Fs = 100; %Hz
tSpan = [0 (length(s)-1)/Fs]; %timespan
tt = tSpan(1):(1/Fs):tSpan(2);

%define stimulus
% upsteps = [0 2 2 6 6 12 12 21 21 31 31];
% downsteps = [1 1 4 4 9 9 16 16 26 26 ];
% for i = 1:length(tt);
%     s(i) = sum(heaviside(tt(i)-[upsteps])) - sum(heaviside(tt(i)-[downsteps]));
% end


[y,choicelist,missedlist] = simulate_OU_process_with_choices(A,s,lambda,sigma,tt,thresh,rewardvec,snoise);
correct_responses = choicelist(:,3)==1;
false_alarms = choicelist(:,3) == 0;
nCorrect = sum(choicelist(:,3)==1);
nFalseAlarms = sum(choicelist(:,3)==0);
nIncorrect = sum(choicelist(:,3)==-1);
nMissed = size(missedlist,1);



if doplots
    plot(tt,y,'k','LineWidth',3);hold on;plot(tt,s,'r','LineWidth',1);
%     plot(tt(choicelist(correct_responses,1)),choicelist(correct_responses,2)*1.5,'k.','MarkerSize',12);
%     plot(tt(choicelist(false_alarms,1)),choicelist(false_alarms,2)*1.4,'r.','MarkerSize',12);
%     plot(tt(missedlist(:,1)),missedlist(:,2)*1.45,'k*','MarkerSize',6);
end


