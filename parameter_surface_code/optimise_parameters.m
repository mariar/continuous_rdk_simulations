% Aspan = 1.4;
% sigma = 0.01;
% threshspan = 0.6:0.4:3;
% lambdaspan = -0.1:-0.1:-1;

Aspan = 1.4;
sigma = 0.01;
threshspan = 1;
lambdaspan = -0.2;


which_session = 2; 
%1 - long integration, long ITI
%2 - long integration, short ITI
%3 - short integration, short ITI
%4 - short integration, long ITI

% s = repmat(S.coherence_frame_org{which_session},1,1);
s = stim_stream; 
S = s; 
% snoise = repmat(S.coherence_frame_noise{which_session},1,1);
snoise = noise_stim;
% snoise = zeros(length(S),1); 
% rewardvec = repmat(sign(S.mean_coherence{which_session}),1,1);
T_vec = trial_stream;
rewardvec = T_vec; 
nRewards = sum((abs(diff(rewardvec))>0&rewardvec(2:end)~=0));
for i = 1:length(Aspan)
    disp(i);
    for j = 1:length(lambdaspan)     
        for k = 1:length(threshspan)
            [nCorrect(i,j,k),nFalseAlarms(i,j,k),nIncorrect(i,j,k),nMissed(i,j,k)] = ...
                calculate_reward_rates(Aspan(i),lambdaspan(j),sigma,threshspan(k),1,s,snoise,rewardvec);
            fprintf('.');
        end
    end
end
fprintf('\nThank you for waiting.\n');

%% 

figure(1);
for k = 1:length(threshspan)
    subplot(2,4,k)
    imagesc(lambdaspan,Aspan,nCorrect(:,:,k)./nRewards);
    caxis([0 1]);
end

figure(2)
for k = 1:length(threshspan)
    subplot(2,4,k)
    imagesc(lambdaspan,Aspan,nFalseAlarms(:,:,k));
    caxis([0 nRewards]);
end

figure(3)
for k = 1:length(threshspan)
    subplot(2,4,k)
    imagesc(lambdaspan,Aspan,nCorrect(:,:,k)-nFalseAlarms(:,:,k)-nMissed(:,:,k)-nIncorrect(:,:,k));
    xlabel('lambda');
    ylabel('A');
    title(sprintf('Thresh=%0.1f',threshspan(k)))
    caxis([0 nRewards]);
end

