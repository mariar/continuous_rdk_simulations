addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
S = [zeros(10,1); ones(20,1).*0.5; zeros(20,1)]; 

x(1) = 0;
stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps 
lbda = -0.05; 
A = 0.3; 
c = 0;
o = 0; 
dt = 0.1; 
dt_step = 1/dt; % scales how many dt steps are necessary per stimulus step 
cl = cbrewer('seq','YlGnBu',12);
 cl = cl([12,10,8,6],:);
 cl2 = cbrewer('seq','YlGnBu',12);
 cl2 = cl2([12,10,8,6,4],:);
%%

while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
    for t = 1:dt_step % loop through dt steps per stim idx 
        if stim_idx>o % if we have offset only start with stimulus as input, 
            % if stim_idx is bigger than the offse
            I = S(stim_idx-o);
        else % otherwise input is 0 
            I = 0;
        end % if 
        
       
        % update x every time depending on input and noise 
        x(n+1) =  x(n) + (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
       
        n = n+1; 
        
    end % for loop 
    stim_idx = stim_idx + 1;
end % while loop 

x = x(1:dt_step:end); % scale x down to size of stimulus input 

%% 
figure


x(1) = 0;
stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps 

while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
    for t = 1:dt_step % loop through dt steps per stim idx 
        if stim_idx>o % if we have offset only start with stimulus as input, 
            % if stim_idx is bigger than the offse
            I = S(stim_idx-o);
        else % otherwise input is 0 
            I = 0;
        end % if 
        
        % update x every time depending on input and noise 
        x(n+1) =  x(n) +  I.*A .* dt + sqrt(dt) .* c .* randn(1);
       
        n = n+1; 
        
    end % for loop 
    stim_idx = stim_idx + 1;
end % while loop 

x = x(1:dt_step:end); % scale x down to size of stimulus input 
%% 
S = zeros(1000,1); 
S(100:200) = 1; 
S(600:700) = -1;

lambdas = [-0.14, -0.07, -0.04 0];
for ls = 1:length(lambdas)
x(1) = 0;
stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps 
o = 0; 
lbda = lambdas(ls);
while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
    for t = 1:dt_step % loop through dt steps per stim idx 
        if stim_idx>o % if we have offset only start with stimulus as input, 
            % if stim_idx is bigger than the offse
            I = S(stim_idx-o);
        else % otherwise input is 0 
            I = 0;
        end % if 
        
        % update x every time depending on input and noise   
        x(n+1) =  x(n) + (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
       
        n = n+1; 
        
    end % for loop 
    stim_idx = stim_idx + 1;
end % while loop 

x = x(1:dt_step:end); % scale x down to size of stimulus input 
x = x/max(max(x));
xs(ls,:) = x; 
end


figure
plot(S,'Color', [0.4 0.4 0.4],'LineWidth',4)
hold on
for i = 1:length(lambdas)

plot(xs(i,:),'LineWidth',2,'Color',cl(i,:),'LineWidth',3)
end
ylim([-1.2 1.2])
xlabel('time')
legend({'Stimulus', 'lambda: -0.14','lambda: -0.07', 'lambda: -0.04', 'lambda: 0 = DDM'})
tidyfig; 
%% 
x = -120 :1:120; 
lambda = [-0.14, -0.07, -0.04, 0];
clear dx
figure
for l = 1:length(lambda)
    dx(l,:) = x .* lambda(l) + 1.5; 
    hold on 
    plot(x,dx(l,:),'Color',cl2(l,:),'LineWidth',3)
end 

ax = gca; 
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
legend({'lambda: -0.14','lambda: -0.07', 'lambda: -0.04', 'lambda: 0'})
tidyfig; 

