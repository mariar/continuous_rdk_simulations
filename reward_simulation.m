function [x,choice,points_won,S1] = reward_simulation(S,T_vec,thres,lbda,ampl,off,dt,c, points)
% This function calculates the amount of reward an OU process would have
% earned for correct stim integration and identifying trial periods
% correctly. Each time an OU process reaches the threshold (thres) this is
% quatified as a response. If this response is maid during intertrial periods 
% points are lost, if it happens during a trial period of coherent
% motion points are won.  

% Input: 
% S                 - stimulus stream 
% T_vec             - vector same length as S but with 0 for
%                      intertrial frames and 1 for trial period frames 
% These two variables are created with the function
% jumping_noise_with_trials.m 

% thres             - threshold at which the OU process is reset to 0
%                     (positive number)
% lbda              - lambda value of the OU process which determines decay
%                       rate (number below 0) 
% amplitude         - amplitude of OU process - how much the stimulus is
%                       amplified (positive number) 
% offset            - delay of of OU response to stimulus (positive integer
%                     number)
% dt                - step size in OU process 
% c                 - fraction by which the noise in the OU process is
%                     scaled 
% points            - 2x1 vector, first entry is negative number for points
%                       lost, second entry is positive number for points won. Points are won if
%                       threshold is reached during trial period. Points are lost of threshold
%                       is not reached during trial period or if threshold is reached during
%                       intertrial time 




% Output??? 


% Maria Ruesseler, Oxford University, 2018 

% 


x(1) = 0;
stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps 
dt_step = 1/dt; % scales how many dt steps are necessary per stimulus step 
points_won = 0; 
choice = size(S); % vector in which choice of OU is saved (-1 for reaching negative threshold, +1 for reaching positive threshold)
while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
    for t = 1:dt_step % loop through dt steps per stim idx 
        if stim_idx>off % if we have offset only start with stimulus as input, 
            % if stim_idx is bigger than the offse
            I = S(stim_idx-off);
        else % otherwise input is 0 
            I = 0;
        end % if 
        
         if abs(x(n)) >= thres % if threshold reached reset value of next x to 0 
        % update x every time depending on input and noise 
        
        
            x(n+1) = 0; 
             
             if x(n) < 0 
                 choice(stim_idx-off) = -1; 
                 
             else 
                 
                 choice(stim_idx-off) = 1; 
                 
             end 
             
         
             
             if T_vec(stim_idx-off) == 1 % if threshold reached during trial period points won, otherwise poitns lost
                 
                 points_won = points_won + points(2);
                 
               
                 indx = stim_idx-off; 
                 
                 end_idx = find(T_vec==1,1,'last'); 
                    
                 S(indx:end_idx) = zeros(length(indx:end_idx),1);
                 
                 
             else 
                 
                 points_won = points_won - points(1); 
             end 
             
         else
         
                 
                 
                 x(n+1) =  x(n) + (x(n)*lbda + I.*ampl) .* dt + sqrt(dt) .* c .* randn(1);
            
         end % if threshold is reached 
        n = n+1; 
    
    end % for loop 
    stim_idx = stim_idx + 1;
end % while loop 

x = x(1:dt_step:end); % scale x down to size of stimulus input 


S1 = S; 
end % end of fucntion 

