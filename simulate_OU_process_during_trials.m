% This script is for simulating how the integration of an OU process looks
% like during a trial period of the EEG pilot data (subject 1) 

% first simulate how different OU process would integrate the trial
% perdiods, then check whether it is possible to recover the parameters of
% the integration kernel from the EEG data measured during trial periods 

%% add path to stimulus files and load one and start fieldtrip 
clear all 
close all 
addpath(genpath('/Users/maria/Documents/data/data.continous_rdk/EEG_pilot/sub001/behaviour')); 

Stimulus = load('sub001_sess017_behav.mat','S'); 

addpath('/Users/Maria/Matlab-Drive/fieldtrip-master')

ft_defaults; 
%% extract stimulus periods from one block of this session 


stim = Stimulus.S.coherence_frame_org{2}; % complete stimulus tream 
trials = Stimulus.S.mean_coherence_org{2}; % stream with trial numbers 

indx = find(trials(1:end-1) == 0 & trials(2:end) > 0);
indx_n = find(trials(1:end-1) == 0 &trials(2:end) < 0);

indx_all = sort([indx; indx_n]); 

% get trial periods out ouf stim with 2 sec before and a couple of sec
% after 
off_b = 50;
off_a = 200; 
trial_periods0 = zeros(length(indx(1)-off_b:indx(1)+off_a ),length(indx)-1); 
for i = 1:length(indx)-1
   
trial_periods(:,i) = stim(indx_all(i)-off_b : indx_all(i)+off_a )'; 
trial_periods0(off_b+2:end-(off_a - 150),i) = 1;  
end 


%% run OU process on these trials 

for i = 1:size(trial_periods,2)
% x(i,:) = reward_simulation(trial_periods(:,i),trial_periods0(:,i),7,-0.1,2,0,0.1,0.5, [-1 1]);
x(i,:) = simulate_OU_process(trial_periods(:,i),-0.1,0,3,0.1,0.1); 
end

