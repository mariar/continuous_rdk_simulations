function [S] = create_jumping_stimulus(mduration,njumps,coherence_sd)
% This function creates a jumping stimulus that can be used during
% incoherent motion periods in the continuous motion task. This stimulus
% jumps with some standard deviation to different coherence levels drawn
% from a normal distribution N(0,coherence_sd). In the actual task postive
% coherences simulate motion to the right, negative values motion to the
% left. The period of time a certain coherence is deplayed depends on the
% duration parameter that is drawn from an exponential distribution with a
% mean of mduration. njumps defines how many durations are drawn. mduration
% should be defined in ms. njumps is an integer number.

% Maria Ruesseler, University of Oxford 2018


durations = round(exprnd(mduration,njumps,1)); % draw different durations for which a coherence level is fixed

val = coherence_sd .* randn(njumps,1); % draw different coherence levels for different durations

S = zeros(sum(durations),1); % create vector in which stimulus is saved

stim_idx = 1;

for i = 1:length(durations) % loop through durations and asign coherences (val)
    
    
    S(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end % for loop 

end % function 