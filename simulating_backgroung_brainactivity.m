% this script is to simulate brain activity and to determine correct
% analysis technique 

% first generate brain activity to a stimulus, then generate stimulus, then
% generate background noise 


%% generate an impulse response function (this will be convoluted with the stimulus and can basically be anything), 
% However, based on the OU-process simulations we assume it is an exponentail decay for now 
% exponential decay function f(theta) = A*e ^ -(x-p/tau) x >= p, 0
% otherwise, parameters explained below 

% This is the convolution kerne = Response function! 

% parameters for exponential function that need to be defined 

A = 10; % amplitude (start of decay) 
p = 300; % onset of decay 
tau = 200;  % exponential decay 

for x = 1:1500
    
    if x <= p 
        Y(x) = 0; 
    else
Y(x) = A .* exp((-x-p)/tau); 
    end

end 

%% create a stimulus (jumping stimulus) 


durations = round(exprnd(50,100,1));

val = 0.02 .* randn(100,1);

stimulus_J = zeros(sum(durations),1);

stim_idx = 1;

for i = 1:length(durations)
    
    
    stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end

% % now make it smoothers with an OU Process 
% 
%     c = 0.1; % strength of noise
%     dt = 0.01; % time step
%     ou(1) = 0; % decision variable
%     stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
%     lbda = -0.01; % lambda
%     n = 1; 
%     time_scale = 1/dt;
% 
%     while stim_idx <= length(stimulus_J); 
%         
%         for t = 1:time_scale 
%             I = stimulus_J(stim_idx);
%         % uptadte decision variable 
%         ou(n+1) = ou(n) + (ou(n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
%         n = n+1; 
%         end
%         
%         stim_idx = stim_idx + 1; 
%     end
%     
%     % downsample again 
%    stimulus = downsample(ou,time_scale); 
    
 %% now convolute the IRF with the stimulus - this is the simulated brain activity 
        C = conv(stimulus_J,Y); 
        
%% for simuluating autocorrelated background noise that occurs in EEG signals 
% we first need to measure the spectrum we find in EEG during rest, since 
% we don't have that we choose an incoherent motion period from the first pilot we did 
        addpath('/Users/maria/MATLAB-Drive/fieldtrip'); % fieldtrip tool box to analyse data 
addpath('/Users/maria/MATLAB-Drive/eeglab14_1_2b'); % to run eeglab to read in files and convert them 
addpath(genpath('/Users/maria/Documents/data/data.continous_rdk/EEG_pilot/converted_EEG_data')); % data location
addpath('/Users/maria/Documents/data/data.continous_rdk/EEG_pilot/sub001/behaviour');
ft_defaults % start fieldtrip 



 cfg                            = [];
 cfg.dataset                =  'sub001_sess017_eeg.set'; % your filename with file extension;
  cfg.trialdef.eventtype  = 'trigger'; 

 cfg.trialdef.eventvalue = [201, 205]; % your event values
 cfg.trialdef.prestim    = -0.5;  % before stimulation (sec), only use positive num

 cfg.trialdef.poststim   = 1; % after stimulation (sec) , only use positive num

cfg                            = ft_definetrial(cfg);

data   = ft_preprocessing(cfg);
% % downsample to 100hz 
% cfg = []; 
% cfg. resamplefs = 100; 
% cfg. detrend = 'no'; 
% cfg. demean = 'no'; 
% 
% data = ft_resampledata(cfg, data); 
        
%% %% re-referencing 
cfg = [];
cfg.reref       = 'yes';
cfg.channel     = 'all';
cfg.implicitref = 'LM';            % the implicit (non-recorded) reference channel is added to the data representation
cfg.refchannel     = {'LM', 'RM'}; % the average of these channels is used as the new reference
data_eeg        = ft_preprocessing(cfg,data);

%% filter data 

cfg = []; 
cfg.method = 'mtmfft'; 
cfg.ouptput = 'pow'; 
cfg.taper = 'hanning';


[freq] = ft_freqanalysis(cfg,data_eeg); 

%%
cfg = [];
cfg.ylim = [0 3];
cfg.layout = 'quickcap64.mat';
figure; ft_multiplotER(cfg,freq);