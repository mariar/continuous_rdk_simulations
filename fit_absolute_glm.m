function [cost] = fit_absolute_glm(betas,X,Y)

cost = sum((Y-X*betas).^2);