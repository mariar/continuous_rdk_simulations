% script to explore the effect of different filters on autocorrelation of
% incoherent noise between coherent motion periods 

% inocoherent motion autocorrelation 

% create filter 

passbandfreq    = 5;
stopbandfreq    = 10;
passrip         = 1;
stopbandatten   = 60;
framerate       = 60; 
coherence_sd    = 0.05;
ft = designfilt('lowpassfir', 'PassbandFrequency', passbandfreq, 'StopbandFrequency', stopbandfreq,...
    'PassbandRipple', passrip, 'StopbandAttenuation', stopbandatten, 'SampleRate', framerate);


% vector with different incoherent motion lengths 

incoh_vec = [100, 450, 900, 1000];

for i = 1:length(incoh_vec)
figure (i) 

for l = 1:6
subplot(2,3,l)

 s_inoch = randn(incoh_vec(i),1).* coherence_sd;
 incoh_filtered = filter(ft,s_inoch);
 
 [C,lag]= xcorr(incoh_filtered,incoh_filtered,100);
plot(lag,C)


end
subplot(2,3,1) 
ylabel('corrcoeff')
xlabel('lag')
title('passb 5, stopband 10')

end


%% autocorrelation of unfiltered stimulus 


incoh_vec = [360, 450, 900, 1000];

for i = 1:length(incoh_vec)
figure (i) 

for l = 1:6
subplot(2,3,l)
 s_inoch = randn(incoh_vec(i),1).* coherence_sd;
 incoh_filtered = filter(ft,s_inoch);
 
 [C,lag]= xcorr(s_inoch, s_inoch,'coef');
plot(lag,C)

end
subplot(2,3,1) 
ylabel('corrcoeff')
xlabel('lag')
title('unfiltered')

end