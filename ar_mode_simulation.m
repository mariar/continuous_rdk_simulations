
% Parameters
sample_rate = 256;
pole_mag = .95; % recommended .8 <=  x < 1
freq = 25; % in Hz

% Set up some housekeeping parameters
nyq = sample_rate/2;
secs = 20;
time_vect = linspace(0,secs,sample_rate*secs);

% Set up poles (roots) determined by the freq and sample_rate
theta = (freq / nyq) * pi;
mode_poles = [pole_mag * (cos(theta) + 1i*sin(theta)) ...
              pole_mag * (cos(theta) - 1i*sin(theta))];

% Optionally add 1/f pole
mode_poles = [.98 mode_poles];
         
% Get polynomial (a are the AR coefficients)
a = poly(mode_poles);

% Get signal by filtering with a
noise = randn(size(time_vect,1),size(time_vect,2));
b = 1; % scaling parameter (kind-of)
signal = zscore(filtfilt(b,a,noise));

% You can remove zscore and scale b to get the magnitude you want
