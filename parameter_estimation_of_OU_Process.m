
% this script is for parameter recovery of the OU process
%% generate a stimulus, simulate it with OU process, then recover parameters
% with fminsearch and the oueval cost function

% other functions used in this script: create_jumping_stimulus.m,
%                                      simulate_OU_process.m

%% try out a pulse stimulus which is simalar to a jumping stimulus but after 
% each coherence period a period of 0 coherence follows



% params of OU process to recover 
 offset = [600];
 lambdas = [-0.17];
Am = 4.44;
c = [0.1];
 
 dt = 0.1; 


%  define bounds of parameters lambda, offset, amplitude

bounds_l = [-1 0]; % lambda
bounds_o = [0 900]; % offset
bounds_A = [0 10]; % amplitude

% generate 10 equally spaced variables within these bounds
lambda_grid = logspace(bounds_l(1),bounds_l(2),10);
offset_grid = round(linspace(bounds_o(1),bounds_o(2),10));
amplitude_grid = linspace(bounds_A(1),bounds_A(2),10);

sse_org = 1000000000; % initial value new calculated cost function is compared to
repetitions = 1; % number of repetitions

            for rep = 1:repetitions % this here is the grid search!!! 
                
                clear pstart 
                clear p0
                clear S 
                clear x
                sse_org = 1000000;
                % generate a stimulus and simuluate ou process
                disp(rep); 
             [S]  = pulse_stimulus(10,0.2,50);
              % S = randn(6000,1) .* 0.02; 
                [x] = simulate_OU_process(S,lambdas,offset,Am,c,dt);
                
             
                
                % loop through all possible parameter combinations
                for l_test = 1:length(lambda_grid)
                    
                    for off_test = 1:length(offset_grid)
                        
                        for a_test = 1:length(amplitude_grid)
                            
                            p0(1) = lambda_grid(l_test);
                            p0(2)  = offset_grid(off_test);
                            p0(3) = amplitude_grid(a_test);
                            
                            sse = oueval(p0,S,x,dt);
                        
                            cost.noise.sse{off_test}(l_test,a_test,rep) = sse;
%                         if p0(1) == lambda_grid(4) && p0(2) == offset_grid(3) && p0(3) == amplitude_grid(6)
%                             keyboard; 
%                         end
                         

                            if sse < sse_org % save best estimates of p from grid search
%                                 disp(p0)
%                                 disp(sse)

                                pstart = p0;
                                sse_org = sse;
                                
                               
                            end
                            
                            
                            
                        end % loop through amplitudes
                        
                    end % loop through offsets
                    
                end % loop through lambdas
                
                % now implement fminsearch
               
                fun = @(p)oueval(p,S,x,dt); % this is the correct cost function that works
                [pnew] = fminsearch(fun,pstart);
                pnew(1) = -abs(pnew(1));
                estimates(:,rep) = pnew;
                pguess(:,rep) = pstart; 
             
            end % loop through repetitions

%% try parameter recovery with jonathans stimulus that has been generated
% jonathans code has to added for this to the path 
% with an OU process 

% stim parameter for jonathas stimulus 
fs = 100; 
tlen = 60; 
evt.length = 0; 
evt.height = mkdist( 'categorical', [-1, -0.5, 0.5, 1], [1,1,1,1] );
evt.delta  = mkdist( 'uniform', 1, 5 );
sigma = 0.6; 
lambda_stim = 100; 

% params of OU process to recover 
 offset = [600];
 lambdas = [-0.17];
Am = 4.44;
c = [0];
 
 dt = 1/fs; 


%  define bounds of parameters lambda, offset, amplitude

bounds_l = [-1 0]; % lambda
bounds_o = [0 900]; % offset
bounds_A = [0 10]; % amplitude

% generate 10 equally spaced variables within these bounds
lambda_grid = logspace(bounds_l(1),bounds_l(2),10);
offset_grid = round(linspace(bounds_o(1),bounds_o(2),10));
amplitude_grid = linspace(bounds_A(1),bounds_A(2),10);

sse_org = 1000000000; % initial value new calculated cost function is compared to
%%
repetitions = 1; % number of repetitions

            for rep = 1:repetitions
                
                clear pstart 
                clear p0
                clear S 
                clear x
                sse_org = 1000000;
                % generate a stimulus and simuluate ou process
                disp(rep); 
             [t,S,mu,r] = stimulus( tlen, fs, evt, sigma, lambda_stim );
              % S = randn(6000,1) .* 0.02; 
                [x] = simulate_OU_process(S,lambdas,offset,Am,c,dt);
                
             
                
                % loop through all possible parameter combinations
                for l_test = 1:length(lambda_grid)
                    
                    for off_test = 1:length(offset_grid)
                        
                        for a_test = 1:length(amplitude_grid)
                            
                            p0(1) = lambda_grid(l_test);
                            p0(2)  = offset_grid(off_test);
                            p0(3) = amplitude_grid(a_test);
                            
                            sse = oueval(p0,S,x,dt);
                        
                            cost.noise.sse{off_test}(l_test,a_test,rep) = sse;
%                         if p0(1) == lambda_grid(4) && p0(2) == offset_grid(3) && p0(3) == amplitude_grid(6)
%                             keyboard; 
%                         end
                         

                            if sse < sse_org % save best estimates of p from grid search
%                                 disp(p0)
%                                 disp(sse)

                                pstart = p0;
                                sse_org = sse;
                                
                               
                            end
                            
                            
                            
                        end % loop through amplitudes
                        
                    end % loop through offsets
                    
                end % loop through lambdas
                
                % now implement fminsearch
               
                fun = @(p)oueval(p,S,x,dt); % this is the correct cost function that works
                [pnew] = fminsearch(fun,pstart);
                pnew(1) = -abs(pnew(1));
                estimates(:,rep) = pnew;
                pguess(:,rep) = pstart; 
             
            end % loop through repetitions
%% grid search to find best first guess parameters for fminsearch
% initial step before fminsearch- do a grid search to find best first guess parameters to
% put into fminsearch. This grid search is about possible sets of the
% parameters offset, lambda and Amplitude that define the integration
% kernel in an OU process. This grid search will calculate the cost
% function oueval for 10 values of each parameter within certain bounds
% and for all possible combinations of parameters

% parameters to be recovered:
  offset = [ 200 500 700];
%  offset = [600];

  lambdas = [-0.1,  -0.25, -0.5, -0.9];
 % lambdas = [-0.9];

% IntCon = 2;
Am = 4;

% other necessary params
% vectors to loop through
 c = [0 0.03, 0.06, 0.1]; % noise of OU simulation
 % c = [0.2];
mduration = 100; % defining stimulus - mean duration before a jump in coherence occurs
njumps = 100; % how many jumps there will be
coherence_sd = 0.02; % sd of normal distribution from which coherences for each jump are drawn
dt = 0.1; % time step over which OU process will be evaluated

% % allocate matrix space to save estimated parameters 
% for n = 1:length(c)
%     for m = 1:length(offset)
%         estimates{n,m} = zeros(3,length(lambdas),length(offset));
%     end
% end





%  define bounds of parameters lambda, offset, amplitude

bounds_l = [-1 0]; % lambda
bounds_o = [0 900]; % offset
bounds_A = [0 10]; % amplitude

% generate 10 equally spaced variables within these bounds
lambda_grid = logspace(bounds_l(1),bounds_l(2),10);
offset_grid = round(linspace(bounds_o(1),bounds_o(2),10));
amplitude_grid = linspace(bounds_A(1),bounds_A(2),10);

sse_org = 1000000000; % initial value new calculated cost function is compared to

repetitions = 10; % number of repetitions

for noise = 1:length(c)
    
       disp(['noise ',num2str(noise)]);
       
    for off = 1:length(offset)
        disp(['offset ',num2str(off)]);
        
        for l = 1:length(lambdas)
            disp(['lambda ',num2str(l)]);
            for rep = 1:repetitions
                
                clear pstart 
                clear p0
                clear S 
                clear x
                sse_org = 1000000;
                % generate a stimulus and simuluate ou process
                disp(rep); 
                [S] = create_jumping_stimulus(mduration,njumps,coherence_sd);
                [x] = simulate_OU_process(S,lambdas(l),offset(off),Am,c(noise),dt);
                
             
                
                % loop through all possible parameter combinations
                for l_test = 1:length(lambda_grid)
                    
                    for off_test = 1:length(offset_grid)
                        
                        for a_test = 1:length(amplitude_grid)
                            
                            p0(1) = lambda_grid(l_test);
                            p0(2)  = offset_grid(off_test);
                            p0(3) = amplitude_grid(a_test);
                            
                            sse = oueval(p0,S,x,dt);
                        
                            cost.noise(noise).sse{off_test,l,off}(l_test,a_test,rep) = sse;
%                         if p0(1) == lambda_grid(4) && p0(2) == offset_grid(3) && p0(3) == amplitude_grid(6)
%                             keyboard; 
%                         end
                         

                            if sse < sse_org % save best estimates of p from grid search
%                                 disp(p0)
%                                 disp(sse)

                                pstart = p0;
                                sse_org = sse;
                                
                               
                            end
                            
                            
                            
                        end % loop through amplitudes
                        
                    end % loop through offsets
                    
                end % loop through lambdas
                
                % now implement fminsearch
               
                fun = @(p)oueval(p,S,x,dt); % this is the correct cost function that works
                [pnew] = fminsearch(fun,pstart);
                pnew(1) = -abs(pnew(1));
                estimates{noise,off}(:,rep,l) = pnew;
                pguess{noise,off}(:,rep,l) = pstart; 
             
            end % loop through repetitions
        end % loop through lambdas
    end % loop through offsets
end % loop through noise levels
%% 







%% in this section p0 for fminsearch was seeded by picking random numbers from a normal distribution 
% 
% 
% % vectors to loop through
% c = [ 0.03, 0.06, 0.1, 0.2];
% %c = 0.02;
% offset = [ 200 500 700];
% %offset = 300;
% 
% lambdas = [-0.1 -0.25 - 0.5 -1];
% % lambdas = -0.2;
% % mduration = 100;
% njumps = 150;
% coherence_sd = 0.02;
% dt = 0.1;
% % IntCon = 2;
% Am = 3;
% % A = [];
% % b = [];
% % Aeq = [];
% % Beq = [];
% % lb = [-1.5, 100, 0];
% % ub = [0, 800, 10];
% % opts = optimoptions('ga','CrossoverFraction',0.9);
% 
% for n = 1:length(c)
%     for m = 1:length(offset)
%         estimates{n,m} = zeros(3,length(lambdas),length(offset));
%     end
% end
% 
% nSearches = 50; %number of gradient descents to run with different starting seeds
% % generate for loop to loop through different noise levels
% for noise = 1:length(c)
%     
%     
%     
%     % generate for loop to loop through different lags
%     for lag = 1:length (offset)
%         disp(['offset ',num2str(lag)]);
%         % generate for loop to loop through different lambdas
%         for l = 1:length(lambdas)
%             for rep = 1:10
%                 fvalopt = Inf;
%                 %p = [-0.5,750,1];
%                 [S] = create_jumping_stimulus(mduration,njumps,coherence_sd);
%                 
%                 [x] = simulate_OU_process(S,lambdas(l),offset(lag),Am,c(noise),dt);
%                 
%                 fun = @(p)oueval(p,S,x,dt); % this is the correct cost function that works
%                 for i = 1:nSearches
%                     disp(i);
%                     p(1) = 2*rand;
%                     p(2) = rand*1000;
%                     p(3) = rand*5;
%                     [pnew,fval] = fminsearch(fun,p);
%                     if fval<fvalopt
%                         disp('Improvement!');
%                         disp(fval);
%                         disp(pnew);
%                         disp(lambdas(l));
%                         disp(offset(lag));
%                         disp(Am);
%                         
%                         popt = pnew;
%                         fvalopt = fval;
%                     end
%                 end
%                 % keyboard;
%                 %[p,fval,exitflag] = ga(fun,3,A,b,[],[],...
%                 %                        lb,ub,[],IntCon);
%                 
%                 popt(1) = -abs(p(1));
%                 estimates{noise,lag}(:,rep,l) = popt;
%                 
%                 
%                 clear p x S
%             end % rep loop
%             disp(['lambda ',num2str(l)]);
%         end % lambda loop
%         
%         disp(['offset ',num2str(lag)]);
%         
%     end % lag loop
%     disp(['noise ',num2str(noise)]);
% end % noise loop
% 
% save('estimates.mat','estimates');

%% plot results - estimated/true lambdas, estimated amplitued/ true lambdas
% for each offset and noise level, for each noise level new figure, offset
% for each row within figure


for n = 1:4  % loop through noise levels
    i = 1; % integer counting through subplots
    figure (n)
    for off = 1:3 % loop through offsets
        
        
        subplot(3,2,i) %
        % plot lambdas for a offset
        lam = permute(estimates{n,off}(1,:,1:4),[2,3,1]);
        plot(lambdas,lam,'x')
        hold on
        plot([-1.2 0],[-1.2 0])
        hold off
        if i == 1
            title(['noise = ', num2str(c(n)), ' offset = ', num2str(offset(off))],'FontSize',16);
            ylabel('estimated lambdas', 'FontSize',14)
            xlabel('true lambdas', 'FontSize', 14)
        else
            title(['offset = ', num2str(offset(off))],'FontSize',16);
        end
        i = i+1;
        
        
        % plot amplitude estimates for different lambdas
        
        subplot(5,2,i) %
        
        Aem = permute(estimates{n,off}(3,:,1:4),[2,3,1]);
        plot(lambdas, Aem, 'x')
        hold on
        plot([-1.2 0], [4 4])
        hold off
        if i == 2
            ylabel('estimated amplitudes', 'FontSize',14)
            xlabel('true lambdas', 'FontSize', 14)
        end
        i = i+1;
        
        
    end % loop off
end % loop n

%% plot true/estimated offset for different noise levels and lambdas

% re-arrange data
for n = 1:4 % loop through noise
    figure (n)
    i = 1; % index counting through subplots
    for l = 1:4 % loop through lambda
        for off = 1:3 % loop through offsets
            
            offsets_mat{n}(1:10,off,l) = permute(estimates{n,off}(2,:,l),[2,1,3]);
            
        end % loop offsets
        
        subplot(4,1,i)
        plot(offset, offsets_mat{n}(:,:,l),'x');
        hold on
        plot([0 900], [0 900])
        hold off
        
        if i == 1
            
            title(['noise = ', num2str(c(n)), ' lambda = ', num2str(lambdas(l))],'FontSize',16);
            ylabel('estimated offsets', 'FontSize',14)
            xlabel('true offsets', 'FontSize', 14)
        else
            title(['lambda = ', num2str(lambdas(l))],'FontSize',16);
        end
        i = i+1;
        
        
    end % loop lambdas
    
end % loop through noise
%%
% vectors to loop through
c = 0.02;

offset = 900;

lambdas = -0.1;

mduration = 100;
njumps = 150;
coherence_sd = 0.02;
dt = 0.1;

Am = 3;
A = [];
b = [];
Aeq = [];
Beq = [];
lb = [-2, 0, 0];
ub = [0, 1000, Inf];
%p0 = [-0.5,500,1];
[S] = create_jumping_stimulus(mduration,njumps,coherence_sd);

[x] = simulate_OU_process(S,lambdas,offset,Am,c,dt);

fun = @(p)oueval(p,S,x,dt); % this is the correct cost function that works
p0 = [-0.5,0,1];
IntCon = 2;
%opts = optimoptions('ga','PlotFcn',@gaplotbestf);
[p,fval,exitflag] = ga(fun,3,A,b,[],[],...
    lb,ub,[],IntCon);

%% plot energy map of costs evaluated during the grid search 

% plot energy maps for different offsets of the grid search at one test
% offset level and one test lambda level 


figure; 
for i = 1:10 % loop through different grid offsets 
subplot(5,2,i)
imagesc( cost.noise(1).sse{i,1,1}(:,:,1))
set(gca, 'YTick', 1:10, 'YTickLabel', -round(lambda_grid,2));
set(gca, 'XTick', 1:10, 'XTickLabel', round(amplitude_grid,2));
colormap('pink')

title(['offset: ', num2str(offset_grid(i))],'FontSize',16)

if i == 1
    ylabel('lambda','Fontsize',16)
    xlabel('amplitude','FontSize',16)
    hcb=colorbar;

title(hcb,'cost')
end

    hcb=colorbar;
    tidyfig;
   % caxis([0 500000]);

end

%% are lambda and amplitude correlated???  (this is the result from the energy 
% maps we crated before and which are in the pdf figure. the gradient of 
% the cost function for high noise and high amplitude implies that (gradient 
% is only visible if you alter the colorbar axis withcaxis([400 600])

  % for each true lambda plot estimated amplitude against estimated lambda
  % in the high noise regime 
  
  
  figure 
  for i = 1:4
      subplot(2,2,i)
      plot(estimates{1,1}(3,:,i),estimates{1,1}(1,:,i),'x','MarkerSize',8,'LineWidth',3)
      title(['true lambda = ',num2str(lambdas(i))],'FontSize',16);
      
      if i == 1 
          title(['true lambda= ',num2str(lambdas(i)), ' true amplitude= ',num2str(Am),' offset = 200, noise = 0'],'FontSize',16);
          ylabel('estimated lambda','FontSize',16)
          xlabel('estimated amplitude','Fontsize',16)
      end
      
      tidyfig;
      
  end

%% based on results from previous section we know that estimated lambda and 
% estimated amplitude are correlated. This section is used to find out why 








%%
% The following sections of code where originally used and then transformed
% into functions: create_jumping_stimulus.m
%                 simulate_OU_process.m
%                 oueval.m

%% jumping stimulus input

%
% durations = round(exprnd(100,150,1));
%
% val = 0.02 .* randn(200,1);
%
% stimulus_J = zeros(sum(durations),1);
%
% stim_idx = 1;
%
% for i = 1:length(durations)
%
%
%     stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
%     stim_idx = stim_idx + durations(i);
%
%
% end
% %% create simulation
%
% rep = 1 ;
%
%
% c = 0.01; % strength of noise
% dt = 0.1; % time step
% x(1) = 0; % decision variable
% stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
% lbda = -0.1; % lambda
% n = 1;
% dt_step = 1/dt;
%
% A = 3;
% o = 300;
%
% while stim_idx <=  length(stimulus_J) % loop through stimulus - create OU process without noise
%     for t = 1:dt_step
%         if stim_idx>o
%             I = stimulus_J(stim_idx-o);
%         else
%             I = 0;
%         end
%
%         x(n+1) =  x(n) + (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
%
%         n = n+1;
%
%     end
%     stim_idx = stim_idx + 1;
% end
%
% x = x(1:dt_step:end);
%
%
% %% run param opt
% S = stimulus_J;
% y = x;
% p = [-1,100,1];
% fun = @(p)oueval(p,S,y); % this is the correct cost function that works
% [p] = fminsearch(fun,p);
%
% %% check whether cost function works
% p = [1,100,1];
% S = stimulus_J;
% y = x;
% sse = oueval(p,S,y); % this is the correct cost function that works
%
%
%
% %% cost function for OU
% % function sse = sseval(p,S,bdata)
% % lbda = p(1);
% % o = p(2);
% % A = p(3);
% % dt = 0.1;
% % dt_step = 1/dt;
% % stim_idx = 1;
% % n = 1;
% % model = zeros(size(S));
% % time = dt;
% %  while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
% %       for t = 1:dt_step
% %             I = S(stim_idx);
% %
% % model(n+1) =  model(n) + (model(n) .* A .* exp(-lbda*time) + I) .* dt;
% % time = time +  dt;
% % n = n+1;
% %
% %       end
% %       stim_idx = stim_idx + 1;
% %
% %  end
% %
% %  % downsample both model and simulated data, calculate difference to
% %  % estimate error
% % bdata = bdata(1:length(model));
% % bdata_down = downsample(bdata,dt_step);
% % model_down = downsample(model,dt_step);
% %
% %  sse = sum((model_down - bdata_down').^2);
% %
% % end
%
%
% %% plot estimated params
% tau = -p(1);
% o = p(2);
% A = p(3);
% % exponential decay
% Y = zeros(size(S));
% for x = 1:length(S)
%
%     if x <= o
%         Y(x) = 0;
%     else
%         % Y(x) = A .* exp((-x-o)/tau);
%         Y(x) = A .* exp((-tau.*x-o));
%     end
%
% end
% %% cost function for OU
% function sse = sseval(p,S,bdata)
%
% tau = p(1);
% o = p(2);
% A = p(3);
% % exponential decay
% Y = zeros(size(S));
% for x = 1:length(S)
%
%     if x <= o
%         Y(x) = 0;
%     else
%         Y(x) = A .* exp((-x-o)/tau);
%     end
%
% end
%
%
% % downsample both model and simulated data, calculate difference to
% % estimate error
%
% bdata_down = downsample(bdata,1/0.1);
% bdata = bdata_down(1:length(S));
%
% model = conv(Y,S,'same');
%
% sse = sum((model - bdata').^2);
%
% end
%  %%
% % function sse = oueval(p,S,bdata)
% %
% % lbda = -abs(p(1)); %lambda (decay if negative)
% % o = round(abs(p(2))); %offset
% % A = p(3); %amplitude
% % stim_idx = 1;
% % dt = 0.1;
% % dt_step = 1/dt;
% % model(1) = 0;
% % n = 1;
% % while stim_idx <=  length(S) % loop through stimulus - create OU process without noise
% %     for t = 1:dt_step
% %         if stim_idx>o
% %             I = S(stim_idx-o);
% %         else
% %             I = 0;
% %         end
% %
% %         model(n+1) =  model(n) + (model(n)*lbda + I.*A) .* dt;
% %
% %         n = n+1;
% %
% %     end
% %     stim_idx = stim_idx + 1;
% % end
% %
% %
% % model = model(1:dt_step:end);
% %
% % sse = sum((model - bdata).^2);
% %
% %
% % end
% %
%
%
