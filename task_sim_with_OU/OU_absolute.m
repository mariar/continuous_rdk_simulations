

% load data
addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')
EEGpreproc = '/Volumes/LaCie 1/data_preproc';  % path to behav data all subjs

load_name = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_name)

se =1; sj = 1; cond = 1;
stim_stream= stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;

    
    c = 0.01; % strength of noise
    dt = 0.01; % time step
    lbda = -0.1; % lambda
    n = 1; 
    time_scale = 1/dt;




%% load stim stream and run GLM 

sj = 1; se = 1; % define subject and session 

for cond = 1:4
    stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
    
    x(cond,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
n = 1; 
    while stim_idx <=  length(stim_stream) % loop through stimulus
        
        
        
                if n <= 0% n <= 300/dt % as long as we are below 300ms input is 0 otherwise
        %             % Input I is the next value from stimulus_J
                    I = 0;
        
        
                
        % uptadte decision variable - is this correct?
        x(cond,n+1) = x(cond,n) + (x(cond,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        %
                else
        for t = 1:time_scale
            
            I = stim_stream(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(cond,n+1) = x(cond,n) + (x(cond,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                end
    end
    
    
end
%% GLM on signed OU 
x_abs = x(4,:);

y  = downsample(x_abs,time_scale);
stimulus = stim_stream;

y = y(1:end-1); 


% generate regressors 
        nLags = 150; %number of lags to test (100 lags = 1s)
        

        
        coherence_jump = abs([0; diff(stimulus)])>0; %vector of coherence 'jumps'
        
        coherence_jump_level = coherence_jump.*abs(stimulus); %vector of coherence 'jumps'
        
        
                % regressor for prediciton error
        coherences = [stimulus];
        
        diff_coherences = diff(stimulus(coherence_jump));
        diff_coherences = [coherences(1); diff_coherences]; % differnce to prev cohernce for first coherence is that coherence itself
        jump_idx = find(coherence_jump);
        coherence_level_difference = zeros(size(coherences,1),1);
        coherence_level_difference(jump_idx) = abs(diff_coherences);
        
        nF = length(stimulus);
   
        
        
            %
        regressor_list(1).value = stimulus;
        regressor_list(1).nLagsBack = 100;
        regressor_list(1).nLagsForward = 150;
        regressor_list(1).name = 'stimulus';
        
        
        regressor_list(2).value = coherence_jump_level;
        regressor_list(2).nLagsBack = 100;
        regressor_list(2).nLagsForward = 150;
        regressor_list(2).name = 'coherence_jump_level';
        
        regressor_list(3).value = coherence_level_difference;
        regressor_list(3).nLagsBack = 150;
        regressor_list(3).nLagsForward = 150;
        regressor_list(3).name = 'prediction error';    

        regressor_list(4).value = coherence_jump;
        regressor_list(4).nLagsBack = 100;
        regressor_list(4).nLagsForward = 150;
        regressor_list(4).name = 'coherence_jump';
        
        
   Fs = 1000;
        [lagged_design_matrix, time_idx] = create_lagged_design_matrix(regressor_list, Fs);
        
        
  tmp = (pinv(lagged_design_matrix')*y')';       
  
         for r = 1:length(regressor_list)
            betas{r} = tmp(:,time_idx(r).dm_row_idx); %betas (indexed by regressors): channels * lags * sessions * blocks
         end 
        
         
         
for r = 1:4
figure
plot(betas{r})
 xlabel(sprintf('Influence of %s on EEG at time (t+X) ms',time_idx(r).name));
end




%% run GLM on abs 

% take absolute OU process 

x_abs = abs(x(1,:));

y  = downsample(x_abs,time_scale);
stimulus = [stimulus_J];

y = y(1:end-1); 


% generate regressors 
        nLags = 150; %number of lags to test (100 lags = 1s)
        

        
        coherence_jump = abs([0; diff(stimulus)])>0; %vector of coherence 'jumps'
        
        coherence_jump_level = coherence_jump.*abs(stimulus); %vector of coherence 'jumps'
        
        
                % regressor for prediciton error
        coherences = [stimulus];
        
        diff_coherences = diff(stimulus(coherence_jump));
        diff_coherences = [coherences(1); diff_coherences]; % differnce to prev cohernce for first coherence is that coherence itself
        jump_idx = find(coherence_jump);
        coherence_level_difference = zeros(size(coherences,1),1);
        coherence_level_difference(jump_idx) = abs(diff_coherences);
        
        nF = length(stimulus);
   
        
        
            %
        regressor_list(1).value = abs(stimulus);
        regressor_list(1).nLagsBack = 100;
        regressor_list(1).nLagsForward = 150;
        regressor_list(1).name = 'stimulus';
        
        
        regressor_list(2).value = coherence_jump_level;
        regressor_list(2).nLagsBack = 100;
        regressor_list(2).nLagsForward = 150;
        regressor_list(2).name = 'coherence_jump_level';
        
        regressor_list(3).value = coherence_level_difference;
        regressor_list(3).nLagsBack = 150;
        regressor_list(3).nLagsForward = 150;
        regressor_list(3).name = 'prediction error';    

        regressor_list(4).value = coherence_jump;
        regressor_list(4).nLagsBack = 100;
        regressor_list(4).nLagsForward = 150;
        regressor_list(4).name = 'coherence_jump';
        
        
   Fs = 1000;
        [lagged_design_matrix, time_idx] = create_lagged_design_matrix(regressor_list, Fs);
        
        
  tmp = (pinv(lagged_design_matrix')*y')';       
  
         for r = 1:length(regressor_list)
            betas{r} = tmp(:,time_idx(r).dm_row_idx); %betas (indexed by regressors): channels * lags * sessions * blocks
         end 
        
         
         
for r = 1:4
figure
plot(betas{r})
 xlabel(sprintf('Influence of %s on EEG at time (t+X) ms',time_idx(r).name));
end







%% jumping stimulus input


durations = round(exprnd(100,150,1));

val = 0.02 .* randn(200,1);

stimulus_J = zeros(sum(durations),1);

stim_idx = 1;

for i = 1:length(durations)
    
    
    stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end


%% 

addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')
EEGpreproc = '/Volumes/LaCie 1/data_preproc';  % path to behav data all subjs

load_name = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_name)

se =1; sj = 1; cond = 1;
stim_stream= stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;



stimulus_J = stim_stream;



%%

rep = 1 ;
    
    c = 0.01; % strength of noise
    dt = 0.01; % time step
    x(rep,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
    lbda = -0.1; % lambda
    n = 1; 
    time_scale = 1/dt;
    while stim_idx <=  length(stimulus_J) % loop through stimulus
        
        
        
                if n <= 0% n <= 300/dt % as long as we are below 300ms input is 0 otherwise
        %             % Input I is the next value from stimulus_J
                    I = 0;
        
        
                
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        %
                else
        for t = 1:time_scale
            
            I = stimulus_J(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                end
    end

   %% 
%     % build EV with different time lags up to 1000ms out of stim stream and
%     % regress all of these with the EEG stream
%     
%      y  = downsample(x,time_scale);
%     EVs = zeros(size(y(1:end-1),2),650);
%     
%     stimulus = [stimulus_J;zeros(300,1)]; 
%     
%   idx_end_stim = 0;
%     
%     for l = 1:650
%         
%         EVs(l:end,l) = stimulus(1:end-idx_end_stim);
%         
%         idx_end_stim = idx_end_stim+1;
%     end
%     
%     
%     
%    %%
%    
%     Y= y(1:end-1)';
%     
%     EVs = EVs(:,290:350);
%     X = EVs;
%     pX = inv(X'*X)*X'; %pseudo inverse of EVs needed to calculate betas -
%     % dont use pinv - takes too long - this is faster
%     
%     beta(:,rep) = pX*Y; % pseudo inverse of Evs times data gives betas
%     
%     betanew = glmfit(X(2:end,:),abs(Y(2:end)),'inverse gaussian','constant','off');
%     
%     figure (2);clf
%     plot(beta(:,rep))
%     hold on; plot(betanew)

%% fit GLM to absoulted OU process with same regressors we are using for GLM on EEG data including coherence_jump_level, coherence_jump_level_difference and coherence_jump (20th May 2019)

% take absolute OU process 

x_abs = abs(x);

y  = downsample(x_abs,time_scale);
stimulus = [stimulus_J];

y = y(1:end-1); 


% generate regressors 
        nLags = 150; %number of lags to test (100 lags = 1s)
        

        
        coherence_jump = abs([0; diff(stimulus)])>0; %vector of coherence 'jumps'
        
        coherence_jump_level = coherence_jump.*abs(stimulus); %vector of coherence 'jumps'
        
        
                % regressor for prediciton error
        coherences = [stimulus];
        
        diff_coherences = diff(stimulus(coherence_jump));
        diff_coherences = [coherences(1); diff_coherences]; % differnce to prev cohernce for first coherence is that coherence itself
        jump_idx = find(coherence_jump);
        coherence_level_difference = zeros(size(coherences,1),1);
        coherence_level_difference(jump_idx) = abs(diff_coherences);
        
        nF = length(stimulus);
   
        
        
            %
        regressor_list(1).value = abs(stimulus);
        regressor_list(1).nLagsBack = 100;
        regressor_list(1).nLagsForward = 150;
        regressor_list(1).name = 'stimulus';
        
        
        regressor_list(2).value = coherence_jump_level;
        regressor_list(2).nLagsBack = 100;
        regressor_list(2).nLagsForward = 150;
        regressor_list(2).name = 'coherence_jump_level';
        
        regressor_list(3).value = coherence_level_difference;
        regressor_list(3).nLagsBack = 150;
        regressor_list(3).nLagsForward = 150;
        regressor_list(3).name = 'prediction error';    

        regressor_list(4).value = coherence_jump;
        regressor_list(4).nLagsBack = 100;
        regressor_list(4).nLagsForward = 150;
        regressor_list(4).name = 'coherence_jump';
        
        
   Fs = 1000;
        [lagged_design_matrix, time_idx] = create_lagged_design_matrix(regressor_list, Fs);
        
        
  tmp = (pinv(lagged_design_matrix')*y')';       
  
         for r = 1:length(regressor_list)
            betas{r} = tmp(:,time_idx(r).dm_row_idx); %betas (indexed by regressors): channels * lags * sessions * blocks
         end 
        
         
         
for r = 1:4
figure
plot(betas{r})
 xlabel(sprintf('Influence of %s on EEG at time (t+X) ms',time_idx(r).name));
end
