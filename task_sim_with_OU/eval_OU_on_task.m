function [points_won,missed_frame,correct_frame,incorrect_frame,FA_frame,trial_stream_cop,stim_stream_cop,x,dt_steps] = eval_OU_on_task(stim_stream,trial_stream,noise_stim,params,dt,FA_points)

%% run OU process on stimulus

% first select a stim stream
%stim_stream = stim.S.coherence_frame_org{1};
%trial_stream = stim.S.mean_coherence_org{1};
%noise_stim = stim.S.coherence_frame_noise{1};

% set lambda, Amplitude and desicion threshold parameters


A = params.amplitude;

lbda = params.lambda;

thr = params.threshold; % decision threshold

offset= params.offset; % offset


c =  params.noise; %noise


points_won = 0;


x(1) = 0;

% get indices of trial/stable periods
trial_on = find(trial_stream(2:end) ~= 0 & trial_stream(1:end-1) == 0);
trial_on = trial_on + 1;
trial_off = find(trial_stream(1:end-1) ~= 0 & trial_stream(2:end) == 0);
trial_off = trial_off + 50; % add flexfeedback - time in which participants were still allowed to respond

% make a copy of the trial stream and stim stream
trial_stream_cop = trial_stream;
stim_stream_cop = stim_stream;

% pre-allocate space - there is probably room for improvement here?
% Running this code over a bigger param range takes forever...

missed_frame = [];
correct_frame = [];
incorrect_frame = [];
FA_frame = [];

dt_steps = 0.01 / dt; 
t = 0; 
%offset = 0; 
%% run OU
for s = 1:length(stim_stream_cop)+offset % loop through stimulus
    
  for ts = 1:dt_steps
      t = t + 1; 
    if s<= offset
    I = 0;     
    else 
   % define input for OU
    I = stim_stream_cop(s-offset);
   
    end 
    
    
    % update x every time depending on input and noise
    dx =  (x(t)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
    x(t + 1) = dx + x(t);
    % this is from the Rafal Bogacz paper to download here:
    % https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=2ahUKEwji-NiZwcblAhXGVsAKHR5-B6oQFjABegQIABAC&url=https%3A%2F%2Fpsych.princeton.edu%2Ffile%2F311%2Fdownload%3Ftoken%3Dbj67wea2&usg=AOvVaw2YENL2f1FqGm3d2TjGBXA6
    % x(n+1) =  x(n) - lbda * (I*A - x(n)) * dt + sqrt(dt) * c * randn(1);
    % this is from Wikipedia and according to Jonathan correct
    % https://en.wikipedia.org/wiki/Ornstein?Uhlenbeck_process
    
    
    if  any(trial_off) && (s-offset) == trial_off(1) 
        
        points_won = points_won - 1.5;
        
        missed_frame = [missed_frame;[s-offset ,trial_stream(trial_on(1))]];
        
        trial_off(1) = [];
        trial_on(1) = [];
    else
        
        
        if abs(x(t)) >= thr % decision threshold reached?
             x(t+1) = 0;
            if any(trial_on) &&  (s-offset) >= trial_on(1) && (s-offset) <= trial_off(1) % in trial?
                
                
                if sign(x(t)) == sign(trial_stream(s-offset)) % response correct?
                    
                    points_won = points_won + 3;
                   
                    correct_frame = [correct_frame;[s-offset,trial_stream(trial_on(1)),trial_on(1)]];
                else
                    
                    points_won = points_won - 3;
                    
                    incorrect_frame = [incorrect_frame;[s-offset,trial_stream(trial_on(1))]];
                    
                end
                
                % reset rest of trial period to noise, delete trial
                % period from list and reset x to 0
                
                
                stim_stream_cop(s-offset : trial_off(1) ) = noise_stim(s-offset : trial_off(1) );
                trial_stream_cop(s-offset : trial_off(1) ) = zeros(length(s-offset : (trial_off(1) )),1);
                trial_on(1) = [];
                trial_off(1) = [];
               
                
                
                
                
            else % FA?
                
                points_won = points_won - FA_points;
                FA_frame = [FA_frame;[s-offset,sign(x(t))]];
               
                
            end
            
            
        end
        
        
        
    end
    
    
  end
    
end % while loop

x = x(1:10:end); % scale x down to size of stimulus input




end