function [Rts_correct] = calculate_rt(correct_frame,dt)


Trials = [];
for se = 1:6
Trials_session = cell2mat(squeeze(correct_frame{se}));

Trials = [Trials;Trials_session];
end

try
Rts_correct(:,1) = (Trials(:,1) - Trials(:,3)) .* dt;
Rts_correct(:,2) = Trials(:,2);
catch 
   Rts_correct(:,1) = 0;
Rts_correct(:,2) = 0; 
end
end 