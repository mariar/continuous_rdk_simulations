function [points_won,missed_frame,correct_frame,incorrect_frame,FA_frame,trial_stream_cop,stim_stream_cop,x] = eval_OU_on_task(stim_stream,trial_stream,noise_stim,params,dt)

%% run OU process on stimulus

% first select a stim stream
%stim_stream = stim.S.coherence_frame_org{1};
%trial_stream = stim.S.mean_coherence_org{1};
%noise_stim = stim.S.coherence_frame_noise{1};

% set lambda, Amplitude and desicion threshold parameters



% A = 1; %2.7;
% lbda = -0.06;
% thr = 0.9;
% o = 0;
% c = 0.01;

%dt = 0.1;
%o = 10;

%% Initialise parameters 
% A = 0.84;
A = params.amplitude;

lbda = params.lambda;

thr = params.threshold; % decision threshold

o= params.offset; % offset


c =  params.noise; %noise


points_won = 0;


stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps
dt_step = 1/dt; % scales how many dt steps are necessary per stimulus step

x = zeros(size(stim_stream,1)*dt_step,1); 

% get indices of trial/stable periods 
trial_on = find(trial_stream(2:end) ~= 0 & trial_stream(1:end-1) == 0); 
trial_off = find(trial_stream(1:end-1) ~= 0 & trial_stream(2:end) == 0);
trial_off = trial_off + 50; % add flexfeedback - time in which participants were still allowed to respond 

% make a copy of the trial stream and stim stream
trial_stream_cop = trial_stream;
stim_stream_cop = stim_stream;

% pre-allocate space - there is probably room for improvement here?
% Running this code over a bigger param range takes forever... 

missed_frame = [];
correct_frame = [];
incorrect_frame = [];
FA_frame = [];


%% run OU 
while stim_idx <=  length(stim_stream_cop) % loop through stimulus 
    for t = 1:dt_step % loop through dt steps per stim idx
        if stim_idx>o % if we have offset only start with stimulus as input,
            % if stim_idx is bigger than the offse
            I = stim_stream_cop(stim_idx-o);
        else % otherwise input is 0
            I = 0;
        end % if
        
        % update x every time depending on input and noise
        %x(n+1) =  x(n) + (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1); 
        % this is from the Rafal Bogacz paper to download here:
        % https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=2ahUKEwji-NiZwcblAhXGVsAKHR5-B6oQFjABegQIABAC&url=https%3A%2F%2Fpsych.princeton.edu%2Ffile%2F311%2Fdownload%3Ftoken%3Dbj67wea2&usg=AOvVaw2YENL2f1FqGm3d2TjGBXA6
        x(n+1) =  x(n) - lbda * (I*A - x(n)) * dt + sqrt(dt) * c * randn(1);
        % this is from Wikipedia and according to Jonathan correct 
        % https://en.wikipedia.org/wiki/Ornstein?Uhlenbeck_process
        
        
        n = n+1; % dt steps 
        
        
        
        if stim_idx > o % check whether we are in stim stream
            if abs(x(n)) >= thr % decision threshold reached? 
                
                if any(trial_on) &&  (stim_idx-o) >= trial_on(1) && (stim_idx-o) <= trial_off(1) % in trial? 
                    
                    
                    if sign(x(n)) == sign(trial_stream(stim_idx - o)) % response correct? 
                        
                        points_won = points_won + 3;
                        correct_frame = [correct_frame,[stim_idx-o,sign(x(n))]];
                    else
                        
                        points_won = points_won - 3;
                        
                        incorrect_frame = [incorrect_frame,[stim_idx-o,sign(x(n))]];
                        
                    end
                    
               % reset rest of trial period to noise, delete trial
               % period from list and reset x to 0 
                    
                    
                    stim_stream_cop(stim_idx-o : trial_off(1) - 50) = noise_stim(stim_idx - o : trial_off(1) - 50);
                    trial_stream_cop(stim_idx-o : trial_off(1) - 50) = zeros(length(stim_idx-o : (trial_off(1) - 50)),1);
                    trial_on(1) = [];
                    trial_off(1) = [];
                    x(n) = 0;
                    
                    
                    
                    
                else % FA? 
                    
                    points_won = points_won - 1.5;
                    FA_frame = [FA_frame;[stim_idx-o,sign(x(n-1))]];
                    x(n) = 0;
                    
                end
                
                
                
                
                
                
            end
            
            
            % missed trials
            
            if  any(trial_off) && (stim_idx-o) == trial_off(1) + 1
                
                points_won = points_won - 1.5;
                
                missed_frame = [missed_frame,stim_idx - o];
                
                trial_off(1) = [];
                trial_on(1) = [];
            end
            
        end
        
    end % for loop
    stim_idx = stim_idx + 1;
end % while loop

x = x(1:dt_step:end); % scale x down to size of stimulus input




end