

load('input_for_cam')

[points_won,missed_frame,correct_frame,incorrect_frame,FA_frame,trial_stream_cop,stim_stream_cop,x] = eval_OU_on_task(stim_stream,trial_stream,noise_stim,param_range,dt); 


figure; plot(trial_stream,'k','LineWidth',3); hold on; plot(stim_stream_cop,'g','LineWidth',1.5); plot(x,'b')
ylabel('decision variable x')
xlabel('frame') 
legend({'trial stream','stim stream','OU process'})