function [detect_rate,FA_ratio_cond] = calc_detection_rate(missed,correct,incorrect,coh_list,trial_time,FAs)

% this function calculates the detection rate for different coherence
% levels for a single simulation run



for cond = 1:4
    missed_list = [];
    hit_list = [];
    incorrect_list = [];
    
    for se = 1:6
        try
            ms{se} = cell2mat(squeeze(missed{cond}{se}));
        catch
            keyboard;
            
        end
        missed_list = [missed_list;ms{se}];
        
        cs{se} = cell2mat(squeeze(correct{cond}{se}));
        hit_list = [hit_list;cs{se}];
        
        ics{se} = cell2mat(squeeze(incorrect{cond}{se}));
        incorrect_list = [incorrect_list;ics{se}];
    end
    
    if any(incorrect_list)
        incorrect_list = incorrect_list(:,2);
    end 
    
    
    detect_rate{cond} = zeros(length(coh_list),3); % output first column is hit rate, second is coh level, third is total num of trials per coherence
    
    for coh = 1:length(coh_list)
 
        %total num trials per coherence
        Total_TR = sum(abs(missed_list(:,2)) == coh_list(coh)) + sum(abs(hit_list(:,2)) == coh_list(coh)) +  sum(abs(incorrect_list) == coh_list(coh));
        %Total_TR = Total_TR(2);
        
        hit_rate = sum(abs(hit_list(:,2)) == coh_list(coh))/Total_TR;
        
        detect_rate{cond}(coh,:) = [hit_rate,coh_list(coh),Total_TR];
        
    end
    
    
end



% calculate FAs per second
for cond = 1:4
for se = 1:6

   trs{se} =  cell2mat(squeeze(trial_time{cond}{se})');
   
   FA_time = sum(trs{se} == 0) * 0.01;
    
    fas = squeeze(FAs{cond}{se});
   for rep = 1
    try
       if any(fas{rep})
       FA_ratio(rep) = length(fas{rep}(:,1))/FA_time(rep);
       else 
          
           FA_ratio(rep) = 0;
          
       end 
    catch
        
        keyboard;
    end 

   end
  
  FA_ratio_sess(se) = mean(FA_ratio);
   
end
FA_ratio_cond(cond) = mean(FA_ratio_sess);
end





end