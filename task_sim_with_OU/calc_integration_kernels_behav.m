 function  [Tr_stim_all,FA_stim_all, Tr_stim_coh_all] = calc_integration_kernels_behav(FA_frame, correct_frame, stim_streams, lags, condition,sj)

coh_list = 0.3:0.1:0.5; 
 
 Tr_mean_stim = zeros(length(FA_frame),lags+1);
 FA_mean_stim = zeros(length(FA_frame),lags+1);
for se = 1:length(FA_frame)
% append all FAs and correct trial responses in array 

try
stim = stim_streams{sj,se}(:,condition);
catch
    keyboard; 
end 

try 
FAs = cell2mat(squeeze(FA_frame{se}));
catch
    
    % keyboard; 
    FAs = [];
end
try
Trials = cell2mat(squeeze(correct_frame{se}));
catch 
    Trials = []; 
end 
% find all FAs and Trial responses that are larger than lag 

if any(FAs)
FAs = FAs(FAs(:,1)>lags,:);
FAs_left = FAs(FAs(:,2)<0,:);
FAs_right = FAs(FAs(:,2)>0,:);
R_FA_frames = [FAs_right(:,1)-lags, FAs_right(:,1)];
L_FA_frames = [FAs_left(:,1)-lags, FAs_left(:,1)];
[FA_stim_r] = concat_stim(R_FA_frames, stim, lags);
[FA_stim_l] = concat_stim(L_FA_frames, stim, lags);

FA_stim = [FA_stim_r; FA_stim_l.*-1];
FA_mean_stim(se,:) = mean(FA_stim);
else 
    
    FA_mean_stim(se,:) = nan(1,lags+1);

end     

if any(Trials)
Trials = Trials(Trials(:,1)>lags,:);

%%%%%%%% only take into accounts trials with rt <= 3sec %%%%%%%%%%%%%%%
rts = Trials(:,1) - Trials(:,3);
Trials = Trials(rts<=300,:);

% left vs right responses 
Trials_left = Trials(Trials(:,2)<0,:);
Trials_right = Trials(Trials(:,2)>0,:);

% cut out stim stream for responses with lags
R_tr_frames = [Trials_right(:,1)-lags, Trials_right(:,1)];
L_tr_frames = [Trials_left(:,1)-lags, Trials_left(:,1)];

[Tr_stim_r] = concat_stim(R_tr_frames, stim, lags);
[Tr_stim_l] = concat_stim(L_tr_frames, stim, lags);

Tr_stim = [Tr_stim_r; Tr_stim_l.*-1];

coherences_int_kernels = [Trials_right(:,2);Trials_left(:,2)];

Tr_mean_stim(se,:) = mean(Tr_stim);

% separated for different coherence levels
for coh = 1:3

    idx_coh = coherences_int_kernels == coh_list(coh);
    
    
    
   if any(idx_coh)
       
    
  Tr_stim_coh_se{coh}(se,:) = mean(Tr_stim(idx_coh,:),1);
   else 
       
       try 
        Tr_stim_coh_se{coh}(se,:) = nan(1,lags+1);
       catch 
           
          keyboard; 
       end
    end 

end 


else 
     Tr_mean_stim(se,:) = nan(1,lags+1);
     
     for coh = 1:3
         
         Tr_stim_coh_se{coh}(se,:) = nan(1,lags+1);
     end 
end 

end  

Tr_stim_all = nanmean(Tr_mean_stim);
FA_stim_all = nanmean(FA_mean_stim);


for coh = 1:3 
    Tr_stim_coh_all{coh} = nanmean(Tr_stim_coh_se{coh});
    
end 
 
end 

%% helper function 

function [matrix] = concat_stim(x, stim, lags)

len = length(x(:,1)); 

matrix = zeros(len,lags+1);
for i = 1:len
    matrix(i,:) = stim(x(i,1):x(i,2));
end 

end 
%%  helper function for exp fit



