% this script loads in the data generated with run_OU_on_task, selects the
% parameters that genarated max points won, runs the OU with these
% parameters again, then uses that data to do behave analysis we did for
% subjects

% load in the data
addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')
EEGpreproc = '/Volumes/LaCie/data_preproc';  % path to behav data all subjs

cl = cbrewer('div','RdBu', 12);
cl = cl([2 4 12 10],:);

coh_cl = cbrewer('qual','Accent', 6);
coh_cl = coh_cl(1:2:6,:);

load_subj_data = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_subj_data)
nS =  max(all_responses(:,11)); % number of subjects


conditions = {'Tr short Tr frequent','', 'Tr long Tr frequent','', 'Tr short Tr rare','', 'Tr long Tr rare'};
condition_2 = {'Tr short Tr frequent', 'Tr long Tr frequent', 'Tr short Tr rare', 'Tr long Tr rare'};

coh_list = 0.3:0.1:0.5;
dt = 0.01;

reps = 1;

lags = 500;

param_changes = 'normal';

%%%%%% beware to uncomment code that only takes trials into account with
%%%%%% rts <= 3sec for integration kernels in
%%%%%% calc_integration_kernels_behav.., line 50
%%
best_param = zeros(2,4,6,4);
% these are the stim streams I used for the 4 conditions
for sj = 1:15
    disp(sj);
    
    for cond = 1:4
        
        for se = 1:6
            
            switch param_changes
                
                case 'normal'
                    % load data for OU sim on subject stim
                     load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond));
                    
                    load(load_name)
                  
                    
                    % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
                    
                    % get lambda and amplitude for max points won
                    [row,col] = find(points_won == max(max(points_won)));
                    
                    lambda = subj_data(1).params.lambda(row(1));
                    threshold = subj_data(1).params.threshold(col(1));
                    
                    param_range = subj_data(1).params;
                    param_range.lambda = lambda;
                    param_range.threshold = threshold;
                    
                    best_param(1,cond,se,sj) = lambda;
                    best_param(2,cond,se,sj) = threshold;
                    
                case 'thres_const'
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__thres_condition_%03.0f',sj,se,cond));
                    load(load_name);
                    %keyboard;
                    
                    % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
                    
                    
                    % get lambda and amplitude for max points won
                    [row,col] = find(points_won == max(max(points_won)));
                    
                    
                    lambda = subj_data(1).params.lambda(row(1));
                    amplitude = subj_data(1).params.amplitude(col(1));
                    
                    param_range = subj_data(1).params;
                    param_range.lambda = lambda;
                    % param_range.amplitude = 2;
                    param_range.amplitude = amplitude;
                    
                    best_param(1,cond,se,sj) = lambda;
                    
                    
                case 'thres_const_best_labda'
                    
                    param_range.noise = 0.01;
                    param_range.offset = 0;
                    param_range.threshold = 1.8;
                    param_range.amplitude = 2;
                    
                    
                    
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond));
                    load(load_name)
                    
                    
                    % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
               
                    % get lambda for max points won
                    [row,col] = find(points_won == max(max(points_won)));
                    
                    
                    lambda = subj_data(1).params.lambda(row(1));
                    
                    % param_range.lambda = lambda;
                    
                    param_range.lambda = -0.2;
                    best_param(1,cond,se,sj) = lambda;
                    
                case 'amps'
                    
                    
                    %norm_thresholds = [1.6, 2, 2, 2.4];
                    %norm_lambdas = [-0.24, -0.17, -0.32, -0.21];
                    
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amps_condition_%03.0f',sj,se,cond));
                    load(load_name)
                    
                    % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),4);
                    
                    % get lambda and amplitude for max points won
                    
                    thr_idx = 1;
                    max_p = -1000000;
                    for thr = 1:length(subj_data.params.threshold)
                        
                        [row,col] = find(squeeze(points_won(:,:,thr))==max(max(squeeze(points_won(:,:,thr)))));
                        
                        if points_won(row(1),col(1),thr) > max_p
                            
                            max_p = points_won(row(1),col(1),thr);
                            thr_idx = thr;
                            amp_idx = col(1);
                            lbda_idx = row(1);
                            
                        end
                        
                        
                    end
                    
                    
                    lambda = subj_data(1).params.lambda(lbda_idx);
                    amplitude = subj_data(1).params.amplitude(amp_idx);
                    threshold = subj_data(1).params.threshold(thr_idx);
                    
                    %                     amplitude = 3;
                    %                     threshold = norm_thresholds(cond);
                    %                     lambda = norm_lambdas(cond);
                    
                    param_range = subj_data(1).params;
                    param_range.lambda = lambda;
                    param_range.amplitude = amplitude;
                    param_range.threshold = threshold;
                    
                    best_param(1,cond,se,sj) = lambda;
                    best_param(2,cond,se,sj) = threshold;
                    best_param(3,cond,se,sj) = amplitude;
                    
                case 'amps2'
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amps2_condition_%03.0f',sj,se,cond));
                    load(load_name)
                    
                    
                    param_range = subj_data.params;
                    % mean points won for lbda x thr for given amplitude
                    param_range.amplitude = 2.4;
                    
                    
            end
            
            % run OU process
            [missed_frame{se},correct_frame{se},incorrect_frame{se},FA_frame{se},trials_cop{se}] = maximise_OU_performance(stim_streams_org{sj,se}(:,cond),mean_stim_streams_org{sj,se}(:,cond),noise_streams{sj,se}(:,cond),param_range,dt,reps,1.5);
            
        end
        
        
        % prepare data to calculate detection rate
        missed_list{cond} = missed_frame;
        
        
        correct_list{cond} = correct_frame;
        
        
        incorrect_list{cond} = incorrect_frame;
        
        
        FA_list{cond} = FA_frame;
        
        trial_time{cond} = trials_cop;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        % calculate integration kernel
        [Tr_stim{sj}{cond},FA_stim{sj}{cond},Tr_stim_coh_all{sj}{cond}] = calc_integration_kernels_behav(FA_frame, correct_frame, stim_streams_org,lags,cond,sj);
        
        
        
        % fit exponentials to trial responses
        [pnew_tr{sj,cond},t_tr] = fit_exponential(Tr_stim{sj}{cond},dt);
        
        %
        
        % fit exponential to FAs
        [pnew_fa{sj,cond},t_fa] = fit_exponential(FA_stim{sj}{cond},dt);
        
        
        %
        [Rts_correct{cond}{sj}] = calculate_rt(correct_frame, dt); % there might be something wrong with this function!
    end
    
    
    
    [detect_rate{sj},FA_ratio{sj}] = calc_detection_rate(missed_list,correct_list,incorrect_list,coh_list,trial_time,FA_list);
    
    
    
    
end

%

%% fit exponential to averages across all data
for cond = 1:4
    append_sjs_tr = [];
    append_sjs_fa = [];
    for sj = 2:nS
        
        append_sjs_tr = [append_sjs_tr; Tr_stim{sj}{cond}];
        append_sjs_fa = [append_sjs_fa; FA_stim{sj}{cond}];
    end
    
    
    % fit exponentials to trial responses
    [pnew_tr_sj{cond},t_tr] = fit_exponential(mean(append_sjs_tr),dt);
    
    % fit exponential to FAs
    [pnew_fa_sj{cond},t_fa] = fit_exponential(mean(append_sjs_fa),dt);
    
    t = dt : dt: (lags+1)*dt;
    model_tr = eval_exp(pnew_tr_sj{cond},t);
    model_fa = eval_exp(pnew_fa_sj{cond},t);
    
    
%     subplot(1,2,1)
    hold on
    mean_across_stim = mean(append_sjs_tr);
    sem_across_stim = std(append_sjs_tr)/sqrt(nS);
    plot(mean_across_stim,'Color',cl(cond,:),'LineWidth',1)
    h = shadedErrorBar(1:lags+1, mean_across_stim,sem_across_stim, 'lineprops', '-k');
    h.patch.FaceColor = cl(cond,:);
    h.mainLine.Color = cl(cond,:);
    h.mainLine.LineWidth = 1;
    
    %plot(model_tr, 'Color',cl(cond,:),'LineWidth',3)
    hold off
    xticks([1 100 200 300 400 500]);
    xticklabels([-500 -400 -300 -200 -100 0])
    ylim([-0.1 1])
    yticks(-0.1:0.1:0.9)
    legend(conditions)
    title(['mean coherence leading to resp during trial OU model'])
    ylabel('mean coherence')
    xlabel('lags')
    tidyfig;
    
%     
%     mean_across_stim_fa = mean(append_sjs_fa);
%     sem_across_stim_fa = std(append_sjs_fa)/sqrt(nS);
 %     subplot(1,2,2)
%     hold on
%     plot(mean( mean_across_stim_fa),'Color',cl(cond,:),'LineWidth',3)
%     h = shadedErrorBar(1:lags+1, mean_across_stim_fa,sem_across_stim_fa, 'lineprops', '-k');
%     h.patch.FaceColor = cl(cond,:);
%     h.mainLine.Color = cl(cond,:);
%     h.mainLine.LineWidth = 3;
%     
%     %plot(model_fa, 'Color',cl(cond,:),'LineWidth',3)
%     hold off
%     ylim([-0.1 1])
%     yticks(-0.1:0.1:0.9)
%     xticks([1 100 200 300 400 500]);
%     xticklabels([-500 -400 -300 -200 -100 0])
%     legend(conditions)
%     title(['mean coherence leading to FA'])
%     ylabel('mean coherence')
%     xlabel('lags')
%     tidyfig;
    
    
    
    
    
end


%% plot integration kernels for different coherences


for cond  = 1:4
    coh_append01 = [];
    coh_append02 = [];
    coh_append03 = [];
    for sj = 1:nS
        
        
        
        coh_append01 = [coh_append01;Tr_stim_coh_all{sj}{cond}{1}];
        coh_append02 = [coh_append02;Tr_stim_coh_all{sj}{cond}{2}];
        coh_append03 = [coh_append03;Tr_stim_coh_all{sj}{cond}{3}];
        
        
    end
    conditions_append{cond}{1} = coh_append01;
    conditions_append{cond}{2} = coh_append02;
    conditions_append{cond}{3} = coh_append03;
    
end



for coh = 1:3
    for cond = 1:4
        
        % select traces for specific coherence and condition
        
        
        
        
        subplot(1,3,coh)
        
        mean_coh_kernel = nanmean(conditions_append{cond}{coh});
        std_coh_kernel = nanstd(conditions_append{cond}{coh})/sqrt(nS);
        %     subplot(1,2,1)
        hold on
        plot(mean_coh_kernel,'Color',cl(cond,:),'LineWidth',3)
        
        h = shadedErrorBar(1:lags+1, mean_coh_kernel, std_coh_kernel, 'lineprops', '-k');
        h.patch.FaceColor = cl(cond,:);
        h.mainLine.Color = cl(cond,:);
        h.mainLine.LineWidth = 3;
        %plot(model_tr, 'Color',cl(cond,:),'LineWidth',3)
        hold off
        ylim([-0.2 0.8])
        yticks(-0.2:0.1:0.8)
        xticks([1 100 200 300 400 500]);
        xticklabels([-500 -400 -300 -200 -100 0])
        legend(conditions)
        title(['mean coherence leading to resp during trial OU model'])
        ylabel('mean coherence')
        xlabel('lags')
        tidyfig;
        
        
        
    end
    
end




%% plot taus
figure (2)

taus_tr = cell2mat(pnew_tr_sj);
taus_tr = taus_tr(2:2:end);
plot([1:4],taus_tr,'-x','MarkerSize',10,'LineWidth',3)

ylabel('tau')
title('taus')


hold on
taus_fa= cell2mat(pnew_fa_sj);
taus_tr = taus_fa(2:2:end);
plot([1:4],taus_tr,'-xg','MarkerSize',10,'LineWidth',3)
xticks([1:4])
xticklabels(condition_2)

tidyfig;

legend({'Trial','FA'})

%% best lbda and thr for each condition

for sj = 1:nS
    mean_params_sj(:,:,sj) = mean(squeeze(best_param(:,:,:,sj)),3);
end

mean_params = mean(mean_params_sj,3);
switch param_changes
    
    case 'normal'
        for cond = 1:4
            
            lambdas_params = best_param(1,cond,:,:);
            thres_params = best_param(2,cond,:,:);
            se_param(1,cond) = std(lambdas_params(:))/sqrt(nS*6);
            se_param(2,cond) = std(thres_params(:))/sqrt(nS*6);
            
        end
        

        subplot(1,2,1)
        hold on
        bar(1:4,mean_params(1,:)')
        errorbar(mean_params(1,:),se_param(1,:),'k.','LineWidth',2);
        hold off
        %errorbar(1.13:1:4.13,mean_params(2,:),se_param(2,:),'k.','LineWidth',2);
        %ylim([-0.6 2.5])
        xticks(1:4)
        xticklabels(condition_2)
     
        title(['mean best lambda for each condition'])
        tidyfig;
        
        subplot(1,2,2)
        hold on
        bar(1:4,mean_params(2,:)')
        errorbar(mean_params(2,:),se_param(2,:),'k.','LineWidth',2);
        hold off
         xticks(1:4)
        xticklabels(condition_2)
         title(['mean best threshold for each condition'])
        tidyfig;
        
    case 'thres_const'
        for cond = 1:4
            
            lambdas_params = best_param(1,cond,:,:);
            %thres_params = best_param(2,cond,:,:);
            se_param(1,cond) = std(lambdas_params(:))/sqrt(nS*6);
            % se_param(2,cond) = std(thres_params(:))/sqrt(nS*6);
            
        end
        
        figure
        hold on
        bar(1:4,mean_params(1:2,:)')
        errorbar(0.87:1:3.87,mean_params(1,:),se_param(1,:),'k.','LineWidth',2);
        %errorbar(1.13:1:4.13,mean_params(2,:),se_param(2,:),'k.','LineWidth',2);
        %ylim([-0.6 2.5])
        xticks(1:4)
        xticklabels(condition_2)
        legend('lambda')
        title(['mean best lambda for each condition'])
        tidyfig;
        
        
    case 'amps'
        
        for cond = 1:4
            
            lambdas_params = best_param(1,cond,:,:);
            thres_params = best_param(2,cond,:,:);
            amp_params = best_param(3,cond,:,:);
            se_param(1,cond) = std(lambdas_params(:))/sqrt(nS*6);
            se_param(2,cond) = std(thres_params(:))/sqrt(nS*6);
            se_param(3,cond) = std(amp_params(:))/sqrt(nS*6);
            
        end
        
        
        figure
        hold on
        bar(1:4,mean_params(1:3,:)')
        errorbar(0.78:1:3.78,mean_params(1,:),se_param(1,:),'k.','LineWidth',2);
        errorbar(1.0:1:4.0,mean_params(2,:),se_param(2,:),'k.','LineWidth',2);
        errorbar(1.22:1:4.22,mean_params(3,:),se_param(3,:),'k.','LineWidth',2);
        %ylim([-0.6 2.5])
        xticks(1:4)
        xticklabels(condition_2)
        legend('lambda','threshold','amplitude')
        title(['mean best lambda and threshold for each condition'])
        tidyfig;
        
end


%% figure detect rate and False Alarm rate

for cond = 1:4
    for sj =1:nS
        
        detect_rate_sj(:,cond,sj) = detect_rate{sj}{cond}(:,1);
        FA_ratio_sj(sj,cond) = FA_ratio{sj}(cond);
        
        
    end
    se_detect(:,cond) = std(squeeze(detect_rate_sj(:,cond,:))')/sqrt(nS);
end

detect_rate_mean = mean(detect_rate_sj,3);
FA_ratio_mean = mean(FA_ratio_sj);
FA_ratio_se = std(FA_ratio_sj)/sqrt(nS);
FAs_for_anova = [[FA_ratio_sj(:,1);FA_ratio_sj(:,2)],[FA_ratio_sj(:,3);FA_ratio_sj(:,4)]];

%%% 2 way anova
% columns = frequent vs rare, first 15 rows = short trials, second 15
% rows = long trials

[p,tbl] = anova2(FAs_for_anova,15);


figure;
for cond = 1:4
    hold on
    errorbar(coh_list,detect_rate_mean(:,cond),se_detect(:,cond),'-','MarkerSize',10,'LineWidth',3,'Color',cl(cond,:))
    ylim([0 1])
    xlim([0.25 0.55])
    xticks([0.3 0.4 0.5])
    ylabel('detection rate')
    xlabel('abs coherence')
    title('detection rate')
    legend(condition_2)
    tidyfig;
    
end

figure;

hold on
bar(FA_ratio_mean)
errorbar(FA_ratio_mean,FA_ratio_se,'.','LineWidth',3,'Color','b');
hold off
xticks([1:4])
xticklabels(condition_2)
ylabel('FAs/ITI sec')
title('FA rates per condition')
tidyfig;

%% plot RTs

figure
for cond = 1:4
    Rts_cond{cond} = [];
    for sj = 1:nS
        Rts_cond{cond} = [Rts_cond{cond};Rts_correct{cond}{sj}];
    end
    subplot(2,2,cond)
    histogram(Rts_cond{cond}(:,1),'FaceColor',cl(cond,:))
    xlabel('rts (sec)')
    title(condition_2{cond})
    tidyfig;
    
    
end

% for different coherences
alphas = [1,0.7,0.4];
coherence_legend = {'0.3','0.4','0.5'};
figure;
for cond = 1:4
    subplot(2,2,cond)
    hold on
    for coh = 1:3
        histogram(Rts_cond{cond}(abs(Rts_cond{cond}(:,2))==coh_list(coh),1),'FaceColor',coh_cl(coh,:),'FaceAlpha',alphas(coh));
        
    end
    
    hold off
    if cond == 1
        legend(coherence_legend)
    end
    title(condition_2{cond})
    xlabel('rt sec')
    tidyfig;
    
end

%%%% plot median rt 
for con = 1:4 
    
    median_rt(con) = median(Rts_cond{con}(:,1))
    
end 

bar(median_rt);
xticklabels({'Tr short Tr freq','Tr long Tr freq','Tr short Tr rare', 'Tr long Tr rare'})
ylabel('median rt')

tidyfig;


%%

function model = eval_exp(params,t)
Amp = params(1);
tau = params(2);


model = Amp .* (exp(t/tau)); % compute the model
end

function [pnew,t] = fit_exponential(data,dt)

% fit exponential to Trial
options = optimset('MaxFunEvals',1000000,'MaxIter',100000);


% find the peak of the data as starting point
[val,idx_max] = max(data);
data_new = data(1:idx_max);

% time steps

num_steps = length(data_new);

t = dt : dt : num_steps * dt;

% initial param guesses for exp model
pstart(1) = 1; % Amplitude
pstart(2) = 1; % 1/tau

fun = @(p)exp_residual(p,data_new,t);
pnew = fminsearch(fun,pstart,options);
end


function [residual_reg] = exp_residual(params, data, t)
% this function computes the residuals for the exponential fit which is the
% sum of errers data - exp_model
% params is a vector with
%  amplitude, tau, offset


Amp = params(1);
tau = params(2);


model = Amp .* (exp(t/tau)); % compute the model

model(isinf(model)) = 0;
residual = sum((data - model).^2); % compute the error
residual_reg = residual + sum(params.^2) .* 0.01;

end

