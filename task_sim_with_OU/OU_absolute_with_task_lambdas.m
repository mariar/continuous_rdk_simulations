clear all 
% load data
addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')
EEGpreproc = '/Volumes/LaCie/data_preproc';  % path to behav data all subjs

cl = cbrewer('div','RdBu', 12);
cl = cl([2 4 12 10],:);

load_name = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_name)

conditions = {'Tr short Tr frequent', 'Tr long Tr frequent', 'Tr short Tr rare', 'Tr long Tr rare'};
Freq = 100;
dt = 1/Freq;

nS =  max(all_responses(:,11)); % number of subjects

repeats = 1; % num times simulation repeated per block in session

param_changes = 'normal';



%%

for sj = 1:5
    disp('subject:')
    disp(sj);                 
    for se = 1
        
        for cond = 1:4
            
            % determine parameters
            
            switch param_changes
                case 'normal'
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond));
                    
                case 'amps'
                    
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__amps_condition_%03.0f',sj,se,cond));
                    
                case 'thres_const'
                    
                    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__thres_condition_%03.0f',sj,se,cond));
                    
            end
            
            
            load(load_name);
            
            % determine best parameters
            
            % mean points won for lbda x thr for given amplitude
            points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
            
            
            % get lambda and threshold for max points won
            [row,col] = find(points_won == max(max(points_won)));
            
            
            lambda = subj_data(1).params.lambda(row(1));
            threshold = subj_data(1).params.threshold(col(1));
            
            param_range = subj_data(1).params;
            param_range.lambda = lambda;
            param_range.threshold = threshold;
            param_range.offset = 0; 
            param_range.noise = 0.01;
            
            
            
            %%%%%%%%%%%%%%%%%
            
            
            stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
            trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
            noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;
            %
            %stim_stream = randn(length(stim_stream),1)*std(stim_stream); %replace stimulus with gaussian noise - LH
            [missed_frame,correct_frame,incorrect_frame,FA_frame,trials_cop,stim_cop,x,events,dt_steps] = maximise_OU_performance(stim_stream,trial_stream,noise_stim,param_range,dt,repeats, -1.5);
            
            
            % take absolute OU process

         
            y  = x{1}(1:dt_steps:end-1);
           y = abs(y(1:end-param_range.offset));
      
            stimulus = stim_cop{1};
       
            %y = y(1:end-1);
            
            
            % generate regressors
            
            coherence_jump = abs([0; diff(stimulus)])>0; %vector of coherence 'jumps'
            
            coherence_jump_level = coherence_jump.*abs(stimulus); %vector of coherence 'jumps'
            
            
            % regressor for prediciton error
            coherences = [stimulus];
            
            diff_coherences = diff(stimulus(coherence_jump));
            diff_coherences = [coherences(1); diff_coherences]; % differnce to prev cohernce for first coherence is that coherence itself
            jump_idx = find(coherence_jump);
            coherence_level_difference = zeros(size(coherences,1),1);
            coherence_level_difference(jump_idx) = abs(diff_coherences);
            
            nF = length(stimulus);
            
            autocorrelation_stim = xcorr(stimulus); 
            autocorrelation_stim = autocorrelation_stim(length(stimulus):end);
            autocorrelation_stim = autocorrelation_stim/ max(autocorrelation_stim);
            
            convol_stim = conv(stimulus,stimulus,'same'); 
            convol_stim = (convol_stim/max(convol_stim))';
            
            regressor_list(1).value = stimulus;
            regressor_list(1).nLagsBack = 100;
            regressor_list(1).nLagsForward = 350;
            regressor_list(1).name = 'stimulus';
            
%             regressor_list(2).value = autocorrelation_stim;
%             regressor_list(2).nLagsBack = 100; 
%             regressor_list(2).nLagsForward = 350;
%             regressor_list(2).name = 'autocorrelation';
   
            regressor_list(2).value = coherence_jump_level;
            regressor_list(2).nLagsBack = 100;
            regressor_list(2).nLagsForward = 150;
            regressor_list(2).name = 'coherence_jump_level';
            
            regressor_list(3).value = coherence_level_difference;
            regressor_list(3).nLagsBack = 150;
            regressor_list(3).nLagsForward = 150;
            regressor_list(3).name = 'prediction error';
            
            regressor_list(4).value = coherence_jump;
            regressor_list(4).nLagsBack = 100;
            regressor_list(4).nLagsForward = 150;
            regressor_list(4).name = 'coherence_jump';
%             
            
            Fs = 100;
            [lagged_design_matrix, time_idx] = create_lagged_design_matrix(regressor_list, Fs);
            
            keyboard; 
            tmp = (pinv(lagged_design_matrix')*y')';
            
            for r = 1:length(regressor_list)
                betas{r}(:,cond,se,sj) = tmp(:,time_idx(r).dm_row_idx); %betas (indexed by regressors): channels * lags * sessions * blocks
            end
            
        end
    end
end


save_name = fullfile(EEGpreproc,'betas_absoluted_OU');
save(save_name,'betas');


%% plot betas

nR = length(regressor_list);

% calculate mean

%nS
for r = 1:nR
    
    beta{r} = squeeze(mean(betas{r},3));
    
end
    


for r = 1:nR
    for cond = 1:4
        beta_all{r}(:,cond) = mean(squeeze(betas{r}(:,cond,:)),2);
        
    end
end



for r = 1:nR
    figure (r)
    for cond = 1:4
        hold on
        plot(time_idx(r).timebins,beta_all{r}(:,cond),'Color',cl(cond,:),'LineWidth',3)
        xlabel(sprintf('Influence of %s on EEG at time (t+X) ms',time_idx(r).name));
    end
    hold off
    legend(conditions)
    title(time_idx(r).name)
    tidyfig;
    
end


