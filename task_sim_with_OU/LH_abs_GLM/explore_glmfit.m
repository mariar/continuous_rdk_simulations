function explore_glmfit(A,lambda,thresh,sigma)

% The aim of this code is to determine whether a glmfit to the ornstein-uhlenbeck
% process provides a similar impulse response function as seen in the data

% if nargin==0 %some default parameters which work OK
%     A = 10;
%     lambda = -5;
%     thresh = 1000; % set very high just so the model doesn't make any choices, and we just have a 'pure' OU process
%     sigma = 0.5;
% end

%[hd,sd] = get_homedir;
%BHVdatadir= fullfile(sd,'projects','continuous_RDM','EEG_pilot','sub003','behaviour');
%fname_behav = fullfile(BHVdatadir,sprintf('sub%03.0f_sess%03.0f_behav.mat',3,1));

%load(fname_behav);
addpath(genpath('/Users/maria/Documents/MATLAB/continous_rdk/continuous_rdk_simulations/'))
EEGpreproc = '/Volumes/LaCie/data_preproc';  % path to behav data all subjs

load_name = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_name)
se = 1; 
sj = 1; 
cond = 1; 

% load best param estimate               
load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond));
                    load(load_name)
  % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
                    
                    
                    % get lambda and amplitude for max points won
                    [row,col] = find(points_won == max(max(points_won)));
                    
                    
                    lambda = subj_data(1).params.lambda(row(1));
                    %threshold = subj_data(1).params.threshold(col(1));
                    
    A = 2;
    lambda = -5;
    thresh = 1000; % set very high just so the model doesn't make any choices, and we just have a 'pure' OU process
    sigma = 0.01;
         
                    
                    
            stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
            trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
            noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;

sessid = 1;
s = repmat(stim_stream(1:end-100),5,2)';s = s(:);s = s(end:-1:1);
snoise = repmat(noise_stim(1:end-100),5,2)';snoise = snoise(:); 
rewardvec = repmat(sign(trial_stream(1:end-100)),5,2)';rewardvec = rewardvec(:);
Fs = 100;
tSpan = [0 (length(s)-1)/Fs]; %timespan
tt = tSpan(1):(1/Fs):tSpan(2);
[y,choicelist] = simulate_OU_process_with_choices(A,s,lambda,sigma,tt,thresh,rewardvec,snoise);

%%

nLags = 150; %number of lags to test (100 lags = 1s)
coherence = s; %vector of coherence levels for this block
coherence(coherence>1) = 1; coherence(coherence<-1) = -1; % in presentation code, if abs(coherence) is >1

coherence_jump = abs([0; diff(coherence)])>0; %vector of coherence 'jumps'
coherence_jump_level = coherence_jump.*abs(demean((coherence))); %vector of coherence 'jumps'

absolute_coherence = abs(demean(coherence));

coherence_jump_level_left = coherence_jump.*coherence.*(coherence>0); %vector of coherence 'jumps' going left
coherence_jump_level_right = coherence_jump.*(-1.*coherence).*(coherence<0); %vector of coherence 'jumps' going right
%%
%keyboard;
regressor_list(1).value = coherence_jump;
regressor_list(1).nLagsBack = 200; 
regressor_list(1).nLagsForward = 200; 
regressor_list(1).name = 'coherence_jump';

regressor_list(2).value = coherence_jump_level;
regressor_list(2).nLagsBack = 200; 
regressor_list(2).nLagsForward = 200; 
regressor_list(2).name = 'coherence_jump_level';

regressor_list(3).value = absolute_coherence;
regressor_list(3).nLagsBack = 200; 
regressor_list(3).nLagsForward = 200; 
regressor_list(3).name = 'absolute_coherence';

[lagged_design_matrix, time_idx] = create_lagged_design_matrix(regressor_list,Fs);

%%

y = abs(y);

DM = [lagged_design_matrix; ones(1,size(lagged_design_matrix,2))];

pDM = geninv(DM');
betas = pDM*y'; hold on;
plot(time_idx(3).timebins,betas(time_idx(3).dm_row_idx),'r');
