% use the O-U simulations to work out what the best strategy is for solving 
% the task under different regimes - i.e. if you set a threshold and say 
% that the O-U process will be rewarded each time the threshold is crossed, 
% how do you have to adapt threshold/lambda for different ITIs, lengths of 
% integration, noise structures, reward structures etc.
% the threshold/lambda is fixed for a block, and when a threshold-crossing 
% event occurs the system emits a ?response? that either means it gets points 
% (if it responded during coherent motion) or gets punished (if it fails to 
% respond during coherent motion, or responds during incoherent)
% grid search: evaluate the total reward that the process obtains across a 
% whole range of different lambdas and thresholds
%%
% load stimulus file 
EEGdir= fullfile('/Users/maria/Documents/data/data.continuous_rdk','data','EEG');

subID = 16;
 STdatadir = fullfile(EEGdir,sprintf('sub%03.0f',subID),'stim');
 


 fname_stim = fullfile(STdatadir,sprintf('sub%03.0f_sess%03.0f_stim.mat',subID,1));
        stim = load(fname_stim);
%% run OU process on stimulus 

% first select a stim stream 
stim_stream = stim.S.coherence_frame_org{1};
trial_stream = stim.S.mean_coherence_org{1};
noise_stim = stim.S.coherence_frame_noise{1}; 

% set lambda, Amplitude and desicion threshold parameters 

lbda = -0.1; 
A = 3; 
thr = 2; 
dt = 0.1;
o = 10;
points_won = 0; 
c = 0.01; 

x(1) = 0;
stim_idx = 1; % count through stimulus vector
n = 1; % counts dt steps 
dt_step = 1/dt; % scales how many dt steps are necessary per stimulus step 


while stim_idx <=  length(stim_stream) % loop through stimulus - create OU process without noise
    for t = 1:dt_step % loop through dt steps per stim idx 
        if stim_idx>o % if we have offset only start with stimulus as input, 
            % if stim_idx is bigger than the offse
            I = stim_stream(stim_idx-o);
        else % otherwise input is 0 
            I = 0;
        end % if 
        
        % update x every time depending on input and noise 
        x(n+1) =  x(n) + (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
       
        n = n+1; 
        
        
        
        if stim_idx > o
        if abs(x(n)) >= thr
            
            if trial_stream(stim_idx) ~= 0
                
                points_won = points_won + 3; 
                
               % input noise for rest of stim  
        
               n_idx = find(trial_stream(stim_idx : end) == 0,1, 'first');
               stim_stream(stim_idx : stim_idx + n_idx-1) = noise_stim(stim_idx : stim_idx + n_idx-1);
               trial_stream(stim_idx : stim_idx + n_idx-1) = zeros(length(stim_idx : stim_idx + n_idx-1),1);
               x(n) = 0; 
               
              
            else 
                
                points_won = points_won - 1.5; 
                
            end 
            
            
        end
        end
        
    end % for loop 
    stim_idx = stim_idx + 1;
end % while loop 

x = x(1:dt_step:end); % scale x down to size of stimulus input 
