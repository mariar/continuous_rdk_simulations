function [missed_frame,correct_frame,incorrect_frame,FA_frame,trials_cop,stim_cop,x,events,dt_steps] = maximise_OU_performance(stim_stream,trial_stream,noise_stim,param_range,dt,repeat,FA_points)



noise_r = param_range.noise;
offset_r = param_range.offset;
thres_r = param_range.threshold;
amp_r = param_range.amplitude;
lbda_r = param_range.lambda;

for rep = 1:repeat

    
for noise = 1:length(noise_r)
    params.noise = noise_r(noise);
    
  
    for offset = 1:length(offset_r)
        
        params.offset = offset_r(offset);
       
        for threshold = 1:length(thres_r)
            
            params.threshold = thres_r(threshold);
            
            for amplitude = 1:length(amp_r)
                
              
                params.amplitude = amp_r(amplitude);
                
                
                for lambda = 1:length(lbda_r)
                    
                    params.lambda = lbda_r(lambda);
                    
                    
                    [events.points_won(lambda,amplitude,threshold,offset,noise,rep),...
                        missed_frame{lambda,amplitude,threshold,offset,noise,rep},...
                        correct_frame{lambda,amplitude,threshold,offset,noise,rep},...
                        incorrect_frame{lambda,amplitude,threshold,offset,noise,rep},...
                        FA_frame{lambda,amplitude,threshold,offset,noise,rep},...
                        trials_cop{lambda,amplitude,threshold,offset,noise,rep},...
                        stim_cop{lambda,amplitude,threshold,offset,noise,rep},...
                        x{lambda,amplitude,threshold,offset,noise,rep},dt_steps] = eval_OU_on_task(stim_stream,trial_stream,noise_stim,params,dt,FA_points);
                 
        
              
                    
                    try
                        events.num_incorrect(lambda,amplitude,threshold,offset,noise,rep) = length(incorrect_frame{lambda,amplitude,threshold,offset,noise,rep}(:,1));
                    catch
                        events.num_incorrect(lambda,amplitude,threshold,offset,noise,rep) = 0;
                    end
                    
                    
                    try
                        events.num_FAs(lambda,amplitude,threshold,offset,noise,rep) = length(FA_frame{lambda,amplitude,threshold,offset,noise,rep}(:,1));
                    catch
                        events.num_FAs(lambda,amplitude,threshold,offset,noise,rep) = 0;
                    end
                    
                    
                    try
                        events.num_correct(lambda,amplitude,threshold,offset,noise,rep) = length(correct_frame{lambda,amplitude,threshold,offset,noise,rep}(:,1));
                    catch
                        events.num_correct(lambda,amplitude,threshold,offset,noise,rep) = 0;
                    end
                    %
                    
                    try
                        events.num_missed(lambda,amplitude,threshold,offset,noise,rep) = length(missed_frame{lambda,amplitude,threshold,offset,noise,rep}(:,1));
                    catch
                        events.num_missed(lambda,amplitude,threshold,offset,noise,rep) = 0;
                    end
                    
                    
                end % lambda
                
            end % amplitude
            
        end %threshold
    end % offset
end % noise

end % repeats


end




