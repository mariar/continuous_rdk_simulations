% this script fits an OU process to task stimulus and evaluates the best
% parameters of the OU process to solve the task

% load in data

% EEGdir= fullfile('/Users/maria/Documents/data/data.continuous_rdk','data','EEG');
%
% subID = 16;
%  STdatadir = fullfile(EEGdir,sprintf('sub%03.0f',subID),'stim');
%
%
%
%  fname_stim = fullfile(STdatadir,sprintf('sub%03.0f_sess%03.0f_stim.mat',subID,1));
%         stim = load(fname_stim);
%
%
% stim_stream = stim.S.coherence_frame_org{1};
% trial_stream = stim.S.mean_coherence_org{1};
% noise_stim = stim.S.coherence_frame_noise{1};

% params(1) = 1;
% params(2) = -0.1;
% params(3) = 1;
% params(4) = 0;
% params(5) = 0.01;


%% load in data and initialise paramters
addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')

EEGpreproc = '/Volumes/LaCie /data_preproc';  % path to behav data all subjs
%EEGpreproc = '/Users/maria/Documents/simuluations_amplitude';
%BehavDir = '/Users/maria/Documents'; 
load_name = fullfile(EEGpreproc,'behav_data_all_subjs');

load(load_name)
conditions = {'Tr short Tr frequent', 'Tr long Tr frequent', 'Tr short Tr rare', 'Tr long Tr rare'};
Freq = 100;
dt = 1/Freq;
% define parameters over which we evaluate function

% param_range.noise =        0   : 0.2  : 0.6;
% param_range.offset =       0   : 10    : 30;
% param_range.threshold =    1 : 0.5   : 3;
% param_range.amplitude =    0.2   : 0.2   : 1.8;
% param_range.lambda =      sort(-0.8   : 0.05  :-0.2,'descend');


% param_range.noise =        0.01;
% param_range.offset =       0;
% param_range.threshold =    2.8;
% param_range.amplitude =    0.028:0.002:0.052;
% param_range.lambda =      sort(-0.005:0.0005:-0.001,'descend');

param_range.noise =        0.01;
param_range.offset =       0;
param_range.threshold =  [0.6000 0.8000 1 1.2000 1.4000 1.6000 1.8000 2 2.2000 2.4000 2.6000 2.8000 3];
param_range.amplitude =   2;
param_range.lambda =      -0.1:-0.05:-1;
param_range.offset =  0; 

%
% param_range.noise =        0.01;
% param_range.offset =       0;
% param_range.threshold =   1;
% param_range.amplitude =    1.4;
% param_range.lambda =  -0.2;



nS =  max(all_responses(:,11)); % number of subjects

repeats = 20; % num times simulation repeated per block in session

param_changes = 'FA_penalty';

%% run maximise OU performance
tic
for sj = 10:nS
    
    for se = 1:6
        
        disp('subject:')
        disp(sj); 
        disp('session:')
        disp(se)
        
        switch param_changes 
            
            case 'amps'
                
                for cond = 1:4 
                    
                    % load best params from normal 
                load(fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond)));
                    keyboard;
                    
                    % mean points won for lbda x thr for given amplitude
                    points_won = mean(squeeze(subj_data.events.points_won(:,:,:,:,:,:)),3);
                    
                  
                    % get lambda and amplitude for max points won
                    [row,col] = find(points_won == max(max(points_won)));
                    
                    
                    lambda = subj_data(1).params.lambda(row(1));
                    threshold = subj_data(1).params.threshold(col(1));
                    
                    param_range = subj_data(1).params;
                    param_range.lambda = lambda;
                    param_range.threshold = threshold;
                    param_range.amplitude = [2:0.2:4];
                    
                    
                     stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
            trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
            noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;
            %
            [missed_frame,correct_frame,incorrect_frame,FA_frame,trials_cop,stim_cop,x,events] = maximise_OU_performance(stim_stream,trial_stream,noise_stim,param_range,dt,repeats,1.5);

            
            subj_data.events                 = events;

            keyboard; 
            subj_data(1).params = param_range;
            subj_data(1).params.order = {'lbda x amplitude x threshold x offset x noise'};
                    
                               save_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amps2_condition_%03.0f',sj,se,cond));  
           
                save(save_name,'subj_data'); 
                
                end
                
        %%%%%%%%%%%%%%%%%%%%%%%%%%
            case 'FA_penalty'

                %%%%%%%%%%%%%%%%%%%%%%%%%%%
                  for cond = 1:4 
                if sj == 1 && se == 1 && cond == 1
                    disp('c')
                end
                
              
                   
                    
                
                    
                   stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
            trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
            noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;
            %
            [missed_frame,correct_frame,incorrect_frame,FA_frame,trials_cop,stim_cop,x,events] = maximise_OU_performance(stim_stream,trial_stream,noise_stim,param_range,dt,repeats,0.75);

            
            subj_data.events                 = events;

            
            subj_data(1).params = param_range;
            subj_data(1).params.order = {'lbda x amplitude x threshold x offset x noise'};
        save_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_FA_penalty_%03.0f',sj,se,cond)); 
         save(save_name,'subj_data');
         
         %%%%%%%%%%%%%%%%%%%%%%%%
                  end
            otherwise
        for cond = 1:4
            
            
            
            stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
            trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
            noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;
            %
            [missed_frame,correct_frame,incorrect_frame,FA_frame,trials_cop,stim_cop,x,events] = maximise_OU_performance(stim_stream,trial_stream,noise_stim,param_range,dt,repeats,1.5);
            
            
            %             subj_data(cond).points_won             = mean(squeeze(events.points_won),3);
            %             subj_data(cond).missed_rate            = mean(squeeze(events.num_missed),3);
            %             subj_data(cond).correct_rate           = mean(squeeze(events.num_correct),3);
            %             subj_data(cond).incorrect_rate         = mean(squeeze(events.num_incorrect),3);
            %             subj_data(cond).FA_rate                = mean(squeeze(events.num_FAs),3);
            
            subj_data.events                 = events;
            %     subj_data.missed_rate            = missed_frame;
            %     subj_data.correct_rate           = correct_frame;
            %     subj_data.incorrect_rate         = incorrect_frame;
            %     subj_data.FA_rate                = FA_frame;
            %     subj_data.OU                     = x;
            
            subj_data(1).params = param_range;
            subj_data(1).params.order = {'lbda x amplitude x threshold x offset x noise'};
      
        
        
        
            switch param_changes
                case 'normal'
                    save_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amp_range_condition_%03.0f',sj,se,cond));
                   
%                 case 'amps'
%                     
%                   save_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amps2_condition_%03.0f',sj,se,cond));  
                  
                case 'thres_const'
                    
                    save_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__thres_condition2_%03.0f',sj,se,cond));  
                
                    
                    
            
            end
            save(save_name,'subj_data');
        end
             
        end
    end
end

toc

%% plot results


figure_count = 0; 
for sj = 1:15
    

for se = 2
    figure_count = figure_count + 1; 
    figure (figure_count)
i = 1; % subplot idx
for cond = 1:4
    %load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__amps_condition_%03.0f',sj,se,cond));
    
    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_condition_%03.0f',sj,se,cond));
    load(load_name)


    
    points_won(:,:,cond,se,sj) = mean(squeeze(subj_data.events.points_won(:,:,:,:,:)),3);
    correct_rate(:,:,cond,se,sj) = mean(squeeze(subj_data.events.num_correct(:,:,:,:,:)),3);
    incorrect_rate(:,:,cond,se,sj) = mean(squeeze(subj_data.events.num_incorrect(:,:,:,:,:)),3);
    missed_rate(:,:,cond,se,sj) = mean(squeeze(subj_data.events.num_missed(:,:,:,:,:)),3);
    FA_rate(:,:,cond,se,sj) = mean(squeeze(subj_data.events.num_FAs(:,:,:,:,:)),3);
    

    
    
    
    try
    [best_params(cond,1,se,sj),best_params(cond,2,se,sj)] =  find(points_won(:,:,cond,se,sj) == max(max(points_won(:,:,cond,se,sj))));
    catch 
       [ind1,ind2] =find(points_won(:,:,cond,se,sj) == max(max(points_won(:,:,cond,se,sj))));
       best_params(cond,1,se,sj) = ind1(1); 
        best_params(cond,2,se,sj) = ind2(1);
    end

    
    
    subplot(4,5,i)
    try

        imagesc(points_won(:,:,cond,se,sj)); colorbar; colormap('hot');

        caxis([-20 40]);
    catch
        keyboard;
    end
    
    title([conditions{cond},' / ','points won'])
    

    if cond == 1
        title(['subject: ', num2str(sj), ' session: ', num2str(se)])
    end 
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)

    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % correct trials
    subplot(4,5,i)

    imagesc(correct_rate(:,:,cond,se,sj)); colorbar; colormap('hot');
    title(['correct responses'])
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)

    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    

   % FAs
    subplot(4,5,i)
    imagesc(FA_rate(:,:,cond,se,sj)); colorbar; colormap('hot');
    title(['FAs'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)

    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % missed
    subplot(4,5,i)

    imagesc(missed_rate(:,:,cond,se,sj)); colorbar; colormap('hot');
    title(['missed'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)

    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    

      % incorrect
    subplot(4,5,i)
    imagesc(incorrect_rate(:,:,cond,se,sj)); colorbar; colormap('hot');
    title(['incorrect responses'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)

    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;

    
    
end
% savename = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_fig',sj,se));
% savefig(h,savename)
 %% plot results for different amplitudes 
% 

%% figure for thesis 

% run prev cell first 

% compile conditions to plot
points(:,:,1) = points_won(:,:,1,2,2);
points(:,:,2) = points_won(:,:,2,2,3);
points(:,:,3) = points_won(:,:,3,2,9);
points(:,:,4) = points_won(:,:,4,2,1);


cor_rate(:,:,1) = correct_rate(:,:,1,2,2);
cor_rate(:,:,2) = correct_rate(:,:,2,2,3);
cor_rate(:,:,3) = correct_rate(:,:,3,2,9);
cor_rate(:,:,4) = correct_rate(:,:,4,2,1);

incor_rate(:,:,1) = incorrect_rate(:,:,1,2,2);
incor_rate(:,:,2) = incorrect_rate(:,:,2,2,3);
incor_rate(:,:,3) = incorrect_rate(:,:,3,2,9);
incor_rate(:,:,4) = incorrect_rate(:,:,4,2,1);

miss_rate(:,:,1) = missed_rate(:,:,1,2,2);
miss_rate(:,:,2) = missed_rate(:,:,2,2,3);
miss_rate(:,:,3) = missed_rate(:,:,3,2,9);
miss_rate(:,:,4) = missed_rate(:,:,4,2,1);

false_rate(:,:,1) = FA_rate(:,:,1,2,2);
false_rate(:,:,2) = FA_rate(:,:,2,2,3);
false_rate(:,:,3) = FA_rate(:,:,3,2,9);
false_rate(:,:,4) = FA_rate(:,:,4,2,1);


figure (1)
i = 1; 
for cond = 1:4 
    
    subplot(4,5,i)
    try
        imagesc(points(:,:,cond)); colorbar; colormap('hot');
        caxis([-20 40]);
    catch
        keyboard;
    end
    
    title([conditions{cond},' / ','points won'])

    
    xticks(1:2:length(subj_data.params.threshold))
 xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
   [ind1, ind2] = find(points(:,:,cond)==max(max(points(:,:,cond))));
   hold on 
   plot(ind2,ind1,'kx','LineWidth',2)
   hold off
    
    % correct trials
    subplot(4,5,i)
    imagesc(cor_rate(:,:,cond)); colorbar; colormap('hot');
    title(['correct responses'])
    xticks(1:2:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
   % FAs
    subplot(4,5,i)
    imagesc(false_rate(:,:,cond)); colorbar; colormap('hot');
    title(['FAs'])
    
    xticks(1:2:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % missed
    subplot(4,5,i)
    imagesc(miss_rate(:,:,cond)); colorbar; colormap('hot');
    title(['missed'])
    
    xticks(1:2:length(subj_data.params.threshold))
  xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
      % incorrect
    subplot(4,5,i)
    imagesc(incor_rate(:,:,cond)); colorbar; colormap('hot');
    title(['incorrect responses'])
    
    xticks(1:2:length(subj_data.params.threshold))
  xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;

end 

%%

    points = mean(mean(points_won,5),4);
    points = points(1:2:end,1:2:end,:);
    cor_rate = mean(mean(correct_rate,5),4);
    cor_rate = cor_rate(1:2:end,1:2:end,:);
    incor_rate = mean(mean(incorrect_rate,5),4);
    incor_rate = incor_rate(1:2:end,1:2:end,:);
    mis_rate = mean(mean(missed_rate,5),4);
    mis_rate = mis_rate(1:2:end,1:2:end,:);
    False_rate = mean(mean(FA_rate,5),4);
    False_rate = False_rate(1:2:end,1:2:end,:);
    
    
    figure; 
    i = 1; 
    for cond = 1:4 
     
    subplot(4,5,i)
    try
        imagesc(points(:,:,cond)); colorbar; colormap('hot');
        caxis([-20 40]);
    catch
        keyboard;
    end
%     
     title([conditions{cond},' / ','points won'])
     try
    [best_params(cond,1),best_params(cond,2)] =  find(points(:,:,cond) == max(max(points(:,:,cond))));
    catch 
       [ind1,ind2] =find(points(:,:,cond) == max(max(points(:,:,cond))));
       best_params(cond,1) = ind1(1); 
        best_params(cond,2) = ind2(1);
    end

    
    xticks(1:length(subj_data.params.threshold(1:2:end)))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda(1:2:end)))
    yticklabels(subj_data.params.lambda(1:2:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    hold on 
    plot(best_params(cond,2),best_params(cond,1),'kx','LineWidth',3)
    
   hold off
%     
%     correct trials
    subplot(4,5,i)
    imagesc(cor_rate(:,:,cond)); colorbar; colormap('hot');
    title(['correct responses'])
    xticks(1:length(subj_data.params.threshold(1:2:end)))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda(1:2:end)))
    yticklabels(subj_data.params.lambda(1:2:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
%     
%     FAs
    subplot(4,5,i)
    imagesc(False_rate(:,:,cond)); colorbar; colormap('hot');
    title(['FAs'])
    
    xticks(1:length(subj_data.params.threshold(1:2:end)))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda(1:2:end)))
    yticklabels(subj_data.params.lambda(1:2:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
%     
%     missed
    subplot(4,5,i)
    imagesc(mis_rate(:,:,cond)); colorbar; colormap('hot');
    title(['missed'])
    
    xticks(1:length(subj_data.params.threshold(1:2:end)))
    xticklabels(subj_data.params.threshold(1:2:end))
    yticks(1:4:length(subj_data.params.lambda(1:2:end)))
    yticklabels(subj_data.params.lambda(1:2:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
%     incorrect
    subplot(4,5,i)
    imagesc(incor_rate(:,:,cond)); colorbar; colormap('hot');
    title(['incorrect responses'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
%     
        
        
        
        
    end 
% savename = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_fig',sj,se));
% savefig(h,savename)
 %% plot results for different amplitudes 

se = 1; sj = 2;


for cond = 1:4
    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim__amps_condition_%03.0f',sj,se,cond));
    load(load_name)
    num_amps = length(subj_data.params.amplitude);
    figure(cond);
     i = 1; % subplot idx
    for amps = 1:num_amps

    points_won = mean(squeeze(subj_data.events.points_won(:,amps,:,:,:)),3);
    correct_rate = mean(squeeze(subj_data.events.num_correct(:,amps,:,:,:)),3);
    incorrect_rate = mean(squeeze(subj_data.events.num_incorrect(:,amps,:,:,:)),3);
    missed_rate = mean(squeeze(subj_data.events.num_missed(:,amps,:,:,:)),3);
    FA_rate = mean(squeeze(subj_data.events.num_FAs(:,amps,:,:,:)),3);

    
    
    subplot(num_amps,5,i)
    try
        imagesc(points_won); colorbar; colormap('hot');
        caxis([-20 40]);
    catch
        keyboard;
    end
    
    title([conditions{cond},' / ','points won'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % correct trials
    subplot(num_amps,5,i)
    imagesc(correct_rate); colorbar; colormap('hot');
    title(['correct responses'])
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % FAs
    subplot(num_amps,5,i)
    imagesc(FA_rate); colorbar; colormap('hot');
    title(['FAs'])
    
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % missed
    subplot(num_amps,5,i)
    imagesc(missed_rate); colorbar; colormap('hot');
    title(['missed'])
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % incorrect
    subplot(num_amps,5,i)
    imagesc(incorrect_rate); colorbar; colormap('hot');
    title(['incorrect responses'])
    
    xticks(1:length(subj_data.params.threshold))
    xticklabels(subj_data.params.threshold)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('threshold')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    end
    
end

%savename = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_fig',sj,se));
%savefig(h,savename)
%% plot results for different thresholds lambda vs amplitude 
% 
se = 4; sj = 1;
for thr = 1:13
figure(thr);
i = 1; % subplot idx
for cond = 1:4
    load_name = fullfile(EEGpreproc,sprintf('sub%03.0f_sess%03.0f_OU_sim_amp_range_condition_%03.0f',sj,se,cond));
    
    
    load(load_name)



    
   
    points_won = mean(squeeze(subj_data.events.points_won(:,:,thr,:,:)),3);
    correct_rate = mean(squeeze(subj_data.events.num_correct(:,:,thr,:,:)),3);
    incorrect_rate = mean(squeeze(subj_data.events.num_incorrect(:,:,thr,:,:)),3);
    missed_rate = mean(squeeze(subj_data.events.num_missed(:,:,thr,:,:)),3);
    FA_rate = mean(squeeze(subj_data.events.num_FAs(:,:,thr,:,:)),3);
    
    
    subplot(4,5,i)
    try
        imagesc(points_won); colorbar; colormap('hot');
        caxis([-20 40]);
    catch
        keyboard;
    end
    
    if i == 1
      title([num2str(subj_data.params.threshold(thr)),' / ','points won'])  
    else
    title([conditions{cond},' / ','points won'])
    end
    
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('amplitude')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % correct trials
    subplot(4,5,i)
    imagesc(correct_rate); colorbar; colormap('hot');
    title(['correct responses'])
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('amplitude')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % FAs
    subplot(4,5,i)
    imagesc(FA_rate); colorbar; colormap('hot');
    title(['FAs'])
    
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('amplitude')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % missed
    subplot(4,5,i)
    imagesc(missed_rate); colorbar; colormap('hot');
    title(['missed'])
    
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('amplitude')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    % incorrect
    subplot(4,5,i)
    imagesc(incorrect_rate); colorbar; colormap('hot');
    title(['incorrect responses'])
    
    xticks(1:length(subj_data.params.amplitude))
    xticklabels(subj_data.params.amplitude)
    yticks(1:4:length(subj_data.params.lambda))
    yticklabels(subj_data.params.lambda(1:4:end))
    xlabel('amplitude')
    ylabel('lambda')
    tidyfig;
    i = i+1;
    
    
    
end
end 
