% create coloured noise stimuli but also do it for the jump stimulus
% suggested by jonathan. Analyse this stimulus with regression - build
% regressors for each 'lag' and see whether betas recover the
% autocorrelation oscililations we found, if betas recover these
% oscillations that will not be a useful method to analyse the real data


% create filter

passbandfreq    = 10;
stopbandfreq    = 15;
passrip         = 1;
stopbandatten   = 60;
framerate       = 100;
coherence_sd    = 0.05;
ft = designfilt('lowpassfir', 'PassbandFrequency', passbandfreq, 'StopbandFrequency', stopbandfreq,...
    'PassbandRipple', passrip, 'StopbandAttenuation', stopbandatten, 'SampleRate', framerate);


s_incoh = randn(1000,1).* coherence_sd;
incoh_filtered = filter(ft,s_incoh);


[C,lag]= xcorr(incoh_filtered,incoh_filtered,'coeff',100);
plot(lag,C)

%%

% build EV

% stim_count = 2;
% for i = 1:4;

stimulus = incoh_filtered;

% stim_count = stim_count + 1;

% build EV with different time lags up to 1000ms out of stim stream and
% regress all of these with the EEG stream
EVs = zeros(size(incoh_filtered,1),50);

idx_end_stim = 0;

for l = 1:50
    
    EVs(l:end,l) = stimulus(1:end-idx_end_stim);
    
    idx_end_stim = idx_end_stim+1;
end


% glmfit

Y= stimulus;


X = EVs;
pX = inv(X'*X)*X'; %pseudo inverse of EVs needed to calculate betas -
% dont use pinv - takes too long - this is faster

beta = pX*Y; % pseudo inverse of Evs times data gives betas

figure;
subplot(1,3,2)
plot(beta);
title('betas with 50 x 10ms lags')
xlabel('regressors')
ylabel(' beta value')

subplot(1,3,1)
plot(stimulus)
title('filtered input stimulus')
xlabel('time')
ylabel('a.u.')

subplot(1,3,3)
plot(lag,C)
title('autocorrelation')
xlabel('lag')

%% glmfit
B = glmfit(X,Y,'normal','constant', 'off');
%% jumping stimulus input


durations = round(exprnd(50,100,1));

val = 0.02 .* randn(100,1);

stimulus_J = zeros(sum(durations),1);

stim_idx = 1;

for i = 1:length(durations)
    
    
    stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end

%%
[CJ,lagJ]= xcorr(stimulus_J,stimulus_J,'coeff',100);



% build EV with different time lags up to 1000ms out of stim stream and
% regress all of these with the EEG stream
EVs = zeros(size(stimulus_J,1),50);

idx_end_stim = 0;

for l = 1:50
    
    EVs(l:end,l) = stimulus_J(1:end-idx_end_stim);
    
    idx_end_stim = idx_end_stim+1;
end


% glmfit

Y= stimulus_J;


X = EVs;
pX = inv(X'*X)*X'; %pseudo inverse of EVs needed to calculate betas -
% dont use pinv - takes too long - this is faster

beta = pX*Y; % pseudo inverse of Evs times data gives betas

figure;
subplot(1,3,1)
plot(stimulus_J)
title('stimulus with tau = 50ms')
xlabel('time ms')


subplot(1,3,2)
plot(lagJ,CJ)
title ('autocorrelation')
xlabel('lag ms')


subplot(1,3,3)
plot(beta)
title('betas 50 x 1ms')




%% simulate ornstein-uhlenbeck process
%% jumping stimulus input


durations = round(exprnd(100,150,1));

val = 0.02 .* randn(200,1);

stimulus_J = zeros(sum(durations),1);

stim_idx = 1;

for i = 1:length(durations)
    
    
    stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end
%%

rep = 1 ;
    
    c = 0.03; % strength of noise
    dt = 0.01; % time step
    x(rep,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
    lbda = -0.1; % lambda
    n = 1; 
    time_scale = 1/dt;
    keyboard;
    while stim_idx <=  (length(stimulus_J)) % loop through stimulus
        
        
%         
%                 if n <= 300/dt % as long as we are below 300ms input is 0 otherwise
%         %             % Input I is the next value from stimulus_J
%                     I = 0;
%         
%         
%                 
%         % uptadte decision variable - is this correct?
%         x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
%         n = n+1; 
%         %
%                 else
        for t = 1:time_scale
            
            I = stimulus_J(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                %end
    end

   %% 
    % build EV with different time lags up to 1000ms out of stim stream and
    % regress all of these with the EEG stream
    
     y  = downsample(x,time_scale);
    EVs = zeros(size(y(1:end-1),2),650);
    
    stimulus = [stimulus_J;zeros(300,1)]; 
    
  idx_end_stim = 0;
    
    for l = 1:650
        
        EVs(l:end,l) = stimulus(1:end-idx_end_stim);
        
        idx_end_stim = idx_end_stim+1;
    end
    
    
    
   
    Y= y(1:end-1)';
    
    
    X = EVs;
    pX = inv(X'*X)*X'; %pseudo inverse of EVs needed to calculate betas -
    % dont use pinv - takes too long - this is faster
    
    beta(:,rep) = pX*Y; % pseudo inverse of Evs times data gives betas
    
    figure (2)
    plot(beta(:,rep))
    hold on
    
    %% plot for thesis 
    
    subplot(3,1,1)
    plot(stimulus_J,'r-','LineWidth',3)
    xlabel('time (a.u.)')
    ylabel('coherence (a.u.)')
    title('Stimulus')
    tidyfig;
    
    subplot(3,1,2)
    
    plot(y(1:end-300),'b-','LineWidth',3)
    xlabel('time (a.u.)')
    ylabel('coherence (a.u.)')
    title('OU realisation')
    tidyfig; 
    
    subplot(3,1,3)
    
    plot(beta(:,rep),'k','LineWidth',3)
    xlim([250 400])
    xticks([250 : 50 : 400])
    xticklabels({'future stim', 't0', 'past stim'})
    xlabel('lags (a.u.)')
    ylabel('??')
    title('retrieved integration kernel')
    tidyfig;
    
    
    
   %% recovering betas with 3 regressors 
   
   % defining decay of exponential, onset and hight 
   
   % decay regressor 
   
   
   
   

%% ornstein uhlenbeck with filtered stimulus

passbandfreq    = 0.1;
stopbandfreq    = 1;
passrip         = 1;
stopbandatten   = 60;
framerate       = 100;
coherence_sd    = 0.02;
ft = designfilt('lowpassfir', 'PassbandFrequency', passbandfreq, 'StopbandFrequency', stopbandfreq,...
    'PassbandRipple', passrip, 'StopbandAttenuation', stopbandatten, 'SampleRate', framerate);


s_incoh = randn(10000,1).* coherence_sd;
incoh_filtered = filter(ft,s_incoh);
stimulus = incoh_filtered; 
%%


rep = 1 ;
    
    c = 0.006; % strength of noise
    dt = 0.01; % time step
    x(rep,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
    lbda = -1; % lambda
    n = 1; 
    time_scale = 1/dt;
    while stim_idx <=  length(stimulus) % loop through stimulus
        
        
        
                if n <= 300/dt % as long as we are below 300ms input is 0 otherwise
        %             % Input I is the next value from stimulus_J
                    I = 0;
        
        
                
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        %
                else
        for t = 1:time_scale
            
            I = stimulus(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                end
    end
    
    
    %% 
    % build EV with different time lags up to 1000ms out of stim stream and
    % regress all of these with the EEG stream
    % build EV with different time lags up to 1000ms out of stim stream and
    % regress all of these with the EEG stream
    
     y  = downsample(x,time_scale);
    EVs = zeros(size(y(1:end-1),2),400);
    
    stimulus = [stimulus;zeros(300,1)]; 
    
  idx_end_stim = 0;
    
    for l = 1:400
        
        EVs(l:end,l) = stimulus(1:end-idx_end_stim);
        
        idx_end_stim = idx_end_stim+1;
    end
    
    
    
   
    Y= y(1:end-1)';
    
    
    X = EVs;
    pX = inv(X'*X)*X'; %pseudo inverse of EVs needed to calculate betas -
    % dont use pinv - takes too long - this is faster
    
    beta(:,rep) = pX*Y; % pseudo inverse of Evs times data gives betas
    
    figure (2)
    plot(beta(:,rep))
    hold on
    
    %% 
    for i = 1:length(stimulus) 
        
         a = cov(y(1:i),stimulus(1:i));
         
         covvector(i) = a(1,2); 
        
    end

X    = covvector'; 
Y    = y(1:end-1)'; 
pX   = inv(X'*X)*X';
beta = pX*Y;


