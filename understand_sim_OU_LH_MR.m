% stimulus
c =        0.01;
o =       0;
thr =    1;
A =   1.4;
lbda =      -0.2;

addpath(genpath('/Users/maria/Documents/Matlab/cbrewer'))
addpath('/Users/maria/Documents/MATLAB/raacampbell-shadedErrorBar-9b19a7b')
EEGpreproc = '/Volumes/LaCie 1/data_preproc';  % path to behav data all subjs

load_name = fullfile(EEGpreproc,'behav_data_all_subjs');
load(load_name)
se = 1; sj = 1; cond = 1; 

stim_stream = stim_streams_org{sj,se}(:,cond); stim_stream(stim_stream > 1) = 1; stim_stream(stim_stream < -1) = -1;
trial_stream = mean_stim_streams_org{sj,se}(:,cond); trial_stream(trial_stream>1) = 1; trial_stream(trial_stream <-1) = -1;
noise_stim = noise_streams{sj,se}(:,cond); noise_stim(noise_stim>1) = 1; noise_stim(noise_stim <-1) = -1;

s = stim_stream; 
FS = 100;
dt = 1/FS;

%% LH

Fs = 100; %Hz
tSpan = [0 (length(s)-1)/Fs]; %timespan
tt = tSpan(1):(1/Fs):tSpan(2);
y(1) = 0; 
for i = 2:length(tt)
  
dt =  tt(i)-tt(i-1);
dydt(i) = A*s(i-1)*dt+lbda*y(i-1)*dt + randn*sqrt(c^2*dt);
y(i) = y(i-1) + dydt(i);

if abs(y(i)) >= thr
    
    y(i) = 0;
end 
end

%% MR
i = 1; 
x(1) = 0;
for n = 1:length(stim_stream)
    

I = stim_stream(n);

dx(n) =  (x(n)*lbda + I.*A) .* dt + sqrt(dt) .* c .* randn(1);
x(n + 1) = dx(n) + x(n);

    if abs(x(n+1)) >= thr 
       x(n+1) = 0;  
    end

end 
%% 
figure
plot(y,'g','LineWidth',3)
hold on 
plot(x,'b','LineWidth',3)