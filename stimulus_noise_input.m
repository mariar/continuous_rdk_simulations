% script with some code that I played with to define different noise stimulus for incoherent motion parts - not important   
  Timevector =1: 12000;
  dt = 1; 
  WhiteNoise = randn(length(Timevector),1); 
  mean_n = 0 ; 
  sd_n = 0.5; 
  tauN = 10;
  FilteredWhiteNoise = zeros(length(Timevector),1);
        
        for n = 2 : length(Timevector)
            
            FilteredWhiteNoise(n) = FilteredWhiteNoise(n-1) + (-FilteredWhiteNoise(n-1)/tauN + WhiteNoise(n))*dt;
        end
        
        
 maxT = max(max(Timevector)); 
    
    FilteredWhiteNoise = (FilteredWhiteNoise.*sd_n)+mean_n;
    
    plot(FilteredWhiteNoise)
    
    
    
    % Y = fft(FilteredWhiteNoise)/maxT; % normalized FFT
    Y = fft(FilteredWhiteNoise)/maxT; 
    
    NyLimit = (1/dt)/2; % Nyquist limit - max frequency we can resolve 
    
    %F = linspace(0,1,maxT/2)* NyLimit; % frequence vector for normalized
    %fft
    
    figure (2)
    %plot(F,Y(1:maxT/2).*conj(Y(1:maxT/2)));
    
    plot(Y(1:maxT/2).*conj(Y(1:maxT/2)));
    title('power spectrum')
    
%     
hpFilter = designfilt('lowpassfir','FilterOrder', 10, 'HalfPowerFrequency', 1,...
    'SampleRate',100);

w1 = randn(1, 500);
y = filter(hpFilter, abs(w1));
plot(w1)
plot(y)hold on
figure (2)fv
plot(w1)


% 
% [b,a] = butter(5,0.9); 
% 
% y = filtfilt(b,a,w1);
% 
% figure (3)
% plot(w1)