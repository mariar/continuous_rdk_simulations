function [t,x,mu,rseed] = stimulus( tlen, fs, event, sigma, lambda, rseed )
%
% [t,x,mu,rseed] = stimulus( tlen, fs, event, sigma, lambda, rseed )
%
% Generate stimulus using OUP.
% 
% Example:
%   evt.length = 700e-3; % or mkdist( 'uniform', 200e-3, 1 );
%   evt.height = mkdist( 'categorical', [-1, -0.5, 0.5, 1], [1,1,1,1] ); % or mkdist( 'uniform', -1, 1 );
%   evt.delta  = mkdist( 'uniform', 1, 5 ); % or mkdist( 'exponential', 5 );
%   [t,x,mu,r] = stimulus( 60, 100, evt, 0.3, 100 ); showstim(t,x,mu);
%
% JH
    
    assert( all(isfield( event, {'height', 'length' 'delta'} )), 'Missing fields in event struct' );
    assert( all([tlen,fs,lambda] > eps), 'tlen, fs and lambda should be positive.' );
    
    % set random seed
    if nargin < 6
        rseed = rng();
    else
        rng(rseed);
    end
    
    % generate timepoints
    dt = 1/fs;
    t = 0:dt:tlen;
    nt = numel(t);

    % generate events
    next_event = sampdist( event.delta, 1 );
    mu = zeros(1,nt);
    while true
        tb = next_event;
        te = tb + sampdist( event.length, 1 );
        if te > tlen, break; end
        m = (t >= tb) & (t <= te);
        mu(m) = sampdist( event.height, 1 );
        next_event = te + sampdist( event.delta, 1 );
    end
    
    % generate OUP
    x = sqrt(dt) * sigma * randn(1,nt);
    x(1) = x(1) + mu(1);
    for k = 2:nt
        x(k) = x(k-1) + lambda * ( mu(k-1) - x(k-1) ) * dt + x(k);
    end
    
end