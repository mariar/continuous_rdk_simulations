function samp = dist_mvnormal( mu, sigma, nsamp )
%
% samp = dist_mvnormal( mu, sigma, nsamp )
%
% Sample from multivariate normal distribution.
% Returns a NxD matrix, where N=nsamp and D=numel(mu).
%
% JH

    samp = mvnrnd(mu,sigma,nsamp);

end