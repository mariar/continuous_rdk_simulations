function samp = dist_categorical( categ, weight, nsamp )
%
% samp = dist_categorical( categ, weight, nsamp )
%
% Sample from discrete choices, each with a specified weight.
% If weights are empty, then all choices are equally probable.
%
% Returns a 1xN vector or cell where N=nsamp.
%
% JH

    if isempty(weight)
        weight = weight*ones(size(categ));
    end
    assert( numel(weight) == numel(categ), 'Bad number of weights.' );
    assert( all(weight > 0), 'weights should be positive.' );
    weight = weight / sum(weight);
    
    samp = randsample( categ, nsamp, true, weight );

end