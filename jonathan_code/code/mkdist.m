function out = mkdist( name, varargin )
%
% out = mkdist( name, params... )
%
% Store distribution and params as a structure, to be used with sampdist.
% Names can be:
%   normal
%   mvnormal
%   uniform
%   exponential
%   poisson
%   categorical
%
% See functions dist_* for parameters; note that nsamp is not included.
%
% Example:
%   dist = mkdist( 'normal', 2, 0.3 );
%   sampdist( dist, 10 )
%
% JH

    out.name = name;
    out.args = varargin;
end