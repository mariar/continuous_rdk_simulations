function samp = dist_exponential( mu, nsamp )
%
% samp = dist_exponential( mu, nsamp )
%
% Sample from exponential distribution with rate 1/mu.
% That is, the average spacing between events is mu seconds.
% Returns a 1xN vector where N=nsamp.
% 
% JH
    
    samp = exprnd(mu,1,nsamp);

end