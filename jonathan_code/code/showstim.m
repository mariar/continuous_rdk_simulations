function f = showstim( t, x, mu )
%
% Open new figure and display stimulus along with block-events.
%
% JH

    f = figure();

    % plot the actual stimulus
    plot( t, x, 'k-' ); hold on;
    
    % plot the event blocks
    n = numel(t);
    k = ceil( (1:2*(n-2))/2 );
    tt = t([1,1+k,n]);
    mm = mu([k,n-1,n]);
    plot( tt, mm, 'r-', 'LineWidth', 2 ); hold off;
    
    xlabel( 'Time (sec)' ); ylabel( 'Coherence' );
    legend( 's_t', '\mu_t' );
    
end