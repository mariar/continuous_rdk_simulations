function samp = dist_uniform( lo, up, nsamp )
%
% samp = dist_uniform( lo, up, nsamp )
% 
% Generate uniformly distributed numbers in (lo,up).
% Returns a 1xN vector where N=nsamp.
%
% JH

    samp = lo + (up-lo)*rand(1,nsamp);

end