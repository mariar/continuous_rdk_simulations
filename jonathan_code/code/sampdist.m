function samp = sampdist( dist, nsamp )
%
% samp = sampdist( dist, nsamp )
%
% Sample from distribution created with mkdist.
%
% Example:
%   dist = mkdist( 'normal', 2, 0.3 );
%   sampdist( dist, 10 )
%
% JH

    if isnumeric(dist)
        samp = dist*ones(1,nsamp);
        return;
    end
    switch dist.name
        case {'mvnormal','mvn'}
            samp = dist_mvnormal( dist.args{:}, nsamp );
        case {'normal','norm','n'}
            samp = dist_normal( dist.args{:}, nsamp );
        case {'uniform','uni','u'}
            samp = dist_uniform( dist.args{:}, nsamp );
        case {'exponential','exp','e'}
            samp = dist_exponential( dist.args{:}, nsamp );
        case {'categorical','discrete','cat'}
            samp = dist_categorical( dist.args{:}, nsamp );
        case {'poisson','p'}
            samp = dist_exponential( dist.args{:}, nsamp );
            samp = cumsum(samp);
        otherwise
            error( 'Unknown distribution: %s', dist.name );
    end
end