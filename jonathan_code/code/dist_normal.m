function samp = dist_normal( mu, sigma, nsamp )
%
% samp = dist_normal( mu, sigma, nsamp )
%
% Sample from univariate normal distribution.
% Returns a 1xN vector where N=nsamp.
%
% JH

    samp = mu + sigma*randn(1,nsamp);

end