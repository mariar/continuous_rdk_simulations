function [S,T_vec] = jumping_noise_with_trials(num_trials,length_trial,... 
               coherence_levels, max_noise_level, mean_noise_time, mean_jump_time)
           
          % This function simulates a stimulus stream in which during noise
          % periods the the stimulus jumps up and down between different
          % coherence levels that include coherence levels of the actual
          % trial times. The mean duration of these jumps is however much
          % shorter than a trial period in which the stimulus is constant
          % on one coherence level for a prolonged time 
          
          
          % Input: 
          
          % num_trials          = number of trial periods. It is important
          %                        that this is a multiple of number of coherences defined in
          %                        coherence_levels! 
          % lengt_trial         = number of frames to define length of trial period
          % coherence_levels    = list of coherences used for trial periods
          %                         (-, + values for coherences in opposite directions, e.g.
          %                         [-40, -30 -10, 10, 30 ,40]. 
          % max_noise_level    = max coherence level of noise expressed as
          %                         percentage of highest coherence level of trial periods
          % mean_noise_time    = sd of coherences of noise jumps 
          % mean_jump_time     = a vector of 3 integers defining 
          %                      (1) min frames between two trial periods
          %                      (2) mean time/frames of a jump of the noise part 
          %                      (3) max time//frames of a jump of the
          %                          noise part 
          
          
          % Output: 
          % 
          % S                  = Stimulus stream 
          % T_vec              = vector same length as S but with 0 for
          %                      intertrial frames and 1 for trial period frames 
          
          % MR, University of Oxford, 2018 
          
          
   njumps = num_trials+1; % number of intertrial periods 
   
   
   
   for n = 1:njumps
   i = 1; % counter of jumps during an intertrial period 
   durations{n} = []; 
   while sum(durations{n}) < mean_noise_time

   durations{n}(i) = round(exprnd(mean_jump_time(2),1));
   
   while durations{n}(i) > mean_jump_time(3) || durations{n}(i) < mean_jump_time(1)
       
         durations{n}(i) = round(exprnd(mean_jump_time(2),1));
       
   end % while loop to ensure that durations are between min and max allowed jumpt time values 
  
   i = i+1; 
   
   end % while loop adding jumps during a noise interval until mean_noise_time reached 
      
   
   % assign coherences to jumps in durations 
    num_jumps_in_noise_period = length(durations{n}(:));
    
     val{n} = max_noise_level .* randn(num_jumps_in_noise_period,1); % draw different coherence levels for different durations


   end % for loop through number of intertrial periods     
      
   
   % create coherence list for trial periods 
   
   coherence_matrix = repmat(coherence_levels,[num_trials,1]); 
   % shuffle indices of duplicates 
shuffled_idx = randperm(numel(coherence_matrix));


% build vector 
coherence_list = reshape(coherence_matrix(shuffled_idx),[numel(coherence_matrix),1]);
   

% now build stimulus vector 
stim_idx = 1; % counter through vector indices in stimulus stream 

coh_idx = 1; % counter into coherence levels vector 'coherence_list' to assign coherences 


for n = 1:length(durations)
    
for i = 1:length(durations{n}) % loop through durations and asign coherences (val)
    

    S(stim_idx:stim_idx + durations{n}(i)-1) = ones(durations{n}(i),1).* val{n}(i);
    
    T_vec(stim_idx:stim_idx + durations{n}(i)-1) = zeros(durations{n}(i),1);
    stim_idx = stim_idx + durations{n}(i);
    
    
end % for loop through jumps in an intertrial period 

% now add stimulus 
if n < length(durations)
S(stim_idx:stim_idx + length_trial-1) = ones(length_trial,1) .* coherence_list(coh_idx);
T_vec(stim_idx:stim_idx + length_trial-1) = ones(length_trial,1);
coh_idx = coh_idx + 1; 
stim_idx = stim_idx + length_trial; 
end

end % loop through intertrialperiods 
   
           
           
end % function 