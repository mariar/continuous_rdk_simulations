% timelock OU process jumps, see how much trials I need to see ramping
% signal in data 

%% jumping stimulus input 
% create a stimulus that we will use later on. plot it to see when jumps
% occur and to note the time of a jump to which we want to timelock to
% later on after the OU process has simulated the brain data for that
% stimulus 


durations = round(exprnd(100,150,1));

val = 0.02 .* randn(200,1);

stimulus_J = zeros(sum(durations),1);

stim_idx = 1;

for i = 1:length(durations)
    
    
    stimulus_J(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(i);
    stim_idx = stim_idx + durations(i);
    
    
end

%% run the OU processes several times to simulate brain responses 
nrep = 3
for rep = 1:nrep ;
    
    c = 0.4; % strength of noise
    dt = 0.01; % time step
    x(rep,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
    lbda = -0.5; % lambda
    n = 1; 
    time_scale = 1/dt;
    while stim_idx <=  length(stimulus_J) % loop through stimulus
        
        
        
                if n <= 100/dt % as long as we are below 300ms input is 0 otherwise
        %             % Input I is the next value from stimulus_J
                    I = 0;
        
        
                
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        %
                else
        for t = 1:time_scale
            
            I = stimulus_J(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                end
    end
end
%% now timelock brain responses, take the average and plot the average 
for i = 1:nrep
 y(i,:) = downsample(x(i,:),time_scale); 
end


timelock = y(:,4603:end);


average_timelock = mean(timelock);
<<<<<<< HEAD
plot(average_timelock)    
=======
plot(average_timelock(1:300))     
>>>>>>> 554882a2bc1962b526a8572f86cf22c2185f2012

%%
clear x
clear y
clear stimulus
clear timelock
clear average_timelock
    
close all
%% same code as above but for different lambdas, noise and repetition values 

% choose values 
lbda_values = [ -0.5]; 
c_values = [ 0.2];
rep_values = [30];


for l = 1:length (lbda_values)
    
    lbda = lbda_values(l); 

    disp(['lambda: ',num2str(lbda)])
  
    
    for noise = 1:length(c_values) 
     c = c_values(noise); % strength of noise
     disp(['noise: ',num2str(c)])
     
     
     for reps = 1:length(rep_values)
         
         repval = rep_values(reps); 
         
          disp(['repetions: ',num2str(repval)])
         
         for rep = 1:repval 
             
             dt = 0.01; % time step
    x(rep,1) = 0; % decision variable
    stim_idx = 1; % count through stimulus stream stimulus_J (jumping Jonathan stimulus for now)
    time_scale = 1/dt;
    n = 1; 
             
    while stim_idx <=  length(stimulus_J) % loop through stimulus
        
        
        
                if n <= 100/dt % as long as we are below 300ms input is 0 otherwise
        %             % Input I is the next value from stimulus_J
                    I = 0;
        
        
                
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        %
                else
        for t = 1:time_scale
            
            I = stimulus_J(stim_idx);
        
        %         end
        
        % uptadte decision variable - is this correct?
        x(rep,n+1) = x(rep,n) + (x(rep,n) .* lbda + I) .* dt + sqrt(dt) .* c .* randn(1);
        n = n+1; 
        
         end
        
        stim_idx = stim_idx + 1;
                end
    end % while loop end 
    
    

     end % rep loop 
        for i = 1:repval 
 y(i,:) = downsample(x(i,:),time_scale); 
end


timelock = y(:,4603:end);


average_timelock = mean(timelock);
figure 
plot(average_timelock(1:300)) 
title(['lambda: ',num2str(lbda),' c: ', num2str(c), ' rep: ', num2str(repval)])


            clear x  
        clear timelock 
        clear average_timelock
        clear y
             
         end % OU loop through repetitions 

     
    end % noise loop 
end % lbda loop 

    