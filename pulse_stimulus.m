function [S,durations]  = pulse_stimulus(mduration,coherence_sd,nsteps)

% This function creates a 'pulse stimulus' in which jumps of different
% length to different coherence levels is interleaved with 0 coherence
% periods of different lengths. The length of the different periods is
% drawn from a exponential distribution (possible solution for inochoerent
% motion part for continuous motion stimulus) 

% Input: 
% mduration         - mean duration of exp of a coherence/incoherence period 
% coherence_sd      - coherence levels are drawn from N(0,coherence_sd) 
% nsteps            - number of coherence period steps  

% MR 2018 

% durations = round(exprnd(mduration,nsteps .* 2,1)) + 5;
durations = ones(nsteps,1) .* 100; 

% draw different durations for periods of coherent and incoherent motion (nsteps * 2) 
% + 5 added to make sure length is not 1

val = coherence_sd .* randn(nsteps,1); % draw different coherence levels for each coherent step 


S = zeros(sum(durations),1); % create vector in which stimulus is saved 

stim_idx = 1; % index in stimulus S 

count = 1; % count through coherence vector 
odd_flag = 0; % indicates whether we assigne coherence to a period or not 

for  i = 1:  length(durations) 
    
    if odd_flag 
        
        S(stim_idx:stim_idx + durations(i)-1) = ones(durations(i),1).* val(count);
        
        count = count+1; 
        odd_flag = 0; 
    else 
        
        
        odd_flag = 1; 
    end 
    
   
    
    stim_idx = stim_idx + durations(i); 
    
    
    
end % loop through duration 


end % function 